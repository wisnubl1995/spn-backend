import multer, { StorageEngine } from "multer";
import path from "path";
import fs from "fs";

class PhotoStore {
  private basePath: string;

  constructor(basePath: string) {
    this.basePath = basePath;
  }

  private getDestinationPath(context: string): string {
    let basePath;
    if (context.includes("/inventory")) {
      basePath = path.join(__dirname, `../public/inbound`);
    } else if (context.includes("/purchaserequest")) {
      basePath = path.join(__dirname, `../public/purchaseRequest`);
    } else {
      basePath = path.join(__dirname, `../public/outbound`);
    }
    return basePath;
  }

  private generateFileName(file: Express.Multer.File): string {
    const uniqueSuffix = `${Date.now()}-${Math.round(Math.random() * 1e9)}`;
    return `${uniqueSuffix}-${file.originalname.replace(/\s/g, "-")}`;
  }

  private createDestinationDirectory(destinationPath: string): void {
    fs.mkdirSync(destinationPath, { recursive: true });
  }

  public getUploadMiddleware(): multer.Multer {
    const storage: StorageEngine = multer.diskStorage({
      destination: (req, file, cb) => {
        const { id } = req.params;
        const destinationPath = this.getDestinationPath(req.originalUrl);
        this.createDestinationDirectory(destinationPath);
        cb(null, destinationPath);
      },
      filename: (req, file, cb) => {
        const fileName = this.generateFileName(file);
        cb(null, fileName);
      },
    });
    return multer({ storage });
  }
}

const photoStore = new PhotoStore(__dirname);

export const uploadPhoto = photoStore.getUploadMiddleware();
