import { injectable } from "inversify";
import winston, { format } from "winston";
import DailyRotateFile from "winston-daily-rotate-file";

export type LogMessage = string;
export type LogContext = object;

export enum LogLevel {
  DEBUG = "debug",
  INFO = "info",
  WARN = "warn",
  ERROR = "error",
}

@injectable()
export class Logging {
  private static _appName = "server";
  private _logger: winston.Logger;

  constructor() {
    this._logger = this.initializeWinston();
  }

  public log(level: LogLevel, msg: LogMessage, context?: LogContext) {
    this._logger.log(level, msg, { context });
  }

  private initializeWinston() {
    const transports = [this.getFileTransport(LogLevel.INFO)];

    const logger = winston.createLogger({ transports });
    logger.exceptions.handle(
      new DailyRotateFile({
        dirname: "logs",
        filename: `${Logging._appName}-exception-%DATE%.log`,
        zippedArchive: true,
        maxFiles: "30d",
        format: format.combine(format.timestamp(), format.prettyPrint()),
      })
    );

    return logger;
  }

  private getFileTransport(logLevel: LogLevel) {
    return new DailyRotateFile({
      dirname: "logs",
      filename: `${Logging._appName}-${logLevel}-%DATE%.log`,
      zippedArchive: true,
      maxFiles: "30d",
      format: format.combine(format.timestamp(), format.prettyPrint()),
      level: logLevel,
    });
  }
}
