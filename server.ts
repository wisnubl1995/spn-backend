import express, { Express, NextFunction, Request, Response } from "express";
import { DatabaseConnection } from "./config/database";
import { MainRouter } from "./routers/router";
import NotFoundHandler from "./template/handle404";
import { LogLevel, Logging } from "./middleware/logger";
import bodyParser from "body-parser";
import cors from "cors"; // Import cors middleware

class Server {
  private app: Express;
  private logger: Logging;

  constructor(private port: number = 8080) {
    this.app = express();
    this.logger = new Logging();
  }

  async init() {
    try {
      await this.app.use(bodyParser.json());
      await this.initDBConnection();
      await this.setupMiddleware();
      this.setupRoutes();
      this.logger.log(LogLevel.INFO, "Server initialized successfully");
    } catch (error) {
      console.error("Error during server initialization:", error);
      process.exit(1);
    }
  }

  private async initDBConnection() {
    try {
      const dbConnection = DatabaseConnection.getInstance();
      await dbConnection.testConnection();
    } catch (error) {
      console.error("Error during database connection initialization:", error);
      throw error;
    }
  }

  private setupMiddleware() {
    this.app.use(cors());
    this.app.use((req: Request, res: Response, next: NextFunction) => {
      const startTime = new Date().getTime();
      const requestData = {
        method: req.method,
        url: req.originalUrl,
        body: req.body,
        params: req.params,
        query: req.query,
      };

      this.logger.log(LogLevel.INFO, `API Request Started`, { requestData });
      const originalSend = res.send;
      const chunks: Buffer[] = [];

      res.send = function (body?: any): Response<any, Record<string, any>> {
        if (body) {
          chunks.push(Buffer.from(body));
        }
        originalSend.apply(res, arguments as any);
        return res;
      };

      next();

      res.on("finish", () => {
        const endTime = new Date().getTime();
        const responseTime = endTime - startTime;
        const responseBody = Buffer.concat(chunks).toString("utf-8");
        const responseData = {
          httpStatus: res.statusCode,
          method: req.method,
          url: req.originalUrl,
          params: req.params,
          query: req.query,
          body: responseBody,
        };

        this.logger.log(LogLevel.INFO, `API Request Completed`, {
          responseData,
          responseTime,
        });
      });
    });
  }

  private setupRoutes() {
    const mainRouter = new MainRouter();
    this.app.use(mainRouter.getRouter());
    this.app.get("/", (req: Request, res: Response) => res.send("Hello World"));
    this.app.use(Server.handle404);
  }

  async start() {
    const dbConnection = DatabaseConnection.getInstance();

    try {
      await dbConnection.testConnection();
      this.app.listen(this.port, () => {
        console.log(`⚡️[server]: Server is running on port ${this.port}`);
      });
    } catch (error) {
      console.error("Error starting the server:", error);
    }
  }

  private static handle404 = NotFoundHandler.handle;
}

const server = new Server();
server.init().then(() => server.start());

export default server;
