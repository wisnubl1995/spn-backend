import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { MocItemService } from "../services/mocItemService";
import { MOCService } from "../services/mocService";
import { PurchaseOrderService } from "../services/purchaseOrderService";
import { MocController } from "./mocController";
import { PurchaseOrderItemService } from "../services/purchaseOrderItemService";

export class MocItemsController {
  private toolsApi: Tools;
  private service: MocItemService;
  private mocService: MOCService;
  private poService: PurchaseOrderService;
  private mocController: MocController;
  private poItemsService: PurchaseOrderItemService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new MocItemService();
    this.mocService = new MOCService();
    this.poService = new PurchaseOrderService();
    this.mocController = new MocController();
    this.poItemsService = new PurchaseOrderItemService();
  }

  public async update(req: Request, res: Response) {
    const { dataMocItems } = req.body;
    try {
      await Promise.all(
        dataMocItems.map(async (item: any) => {
          const id = item.id;
          const detailItem = {
            shop_one_name: item.shopOneName,
            shop_one_unit_price: item.shopOneUnitPrice,
            shop_one_total_price: item.shopOneTotalPrice,
            shop_one_remarks: item.shopOneRemarks,
            shop_two_name: item.shopTwoName,
            shop_two_unit_price: item.shopTwoUnitPrice,
            shop_two_total_price: item.shopTwoTotalPrice,
            shop_two_remarks: item.shopTwoRemarks,
            shop_three_name: item.shopThreeName,
            shop_three_unit_price: item.shopThreeUnitPrice,
            shop_three_total_price: item.shopThreeTotalPrice,
            shop_three_remarks: item.shopThreeRemarks,
            l2_status: item.l2Status,
            l3_status: item.l3Status,
          };
          await this.service.update(id, detailItem);
        })
      );
      res.status(200).json({ message: "Data Succesfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async moveItemToPO(req: Request, res: Response) {
    const { idMoc, detailMocItems } = req.body;
    try {
      const getMocHeader = await this.mocService.findOne(idMoc);
      if (!getMocHeader) {
        return res
          .status(404)
          .json({ message: "Matrix of Comparison Not Found" });
      }

      if (getMocHeader.status === "Close") {
        return res
          .status(404)
          .json({ message: "Matrix of Comparison is Closed" });
      }
      let ifNotApproved: any = [];
      for (const detailMocItem of detailMocItems) {
        const dataDetailMoc = await this.service.findOne(detailMocItem.id);
        if (
          typeof dataDetailMoc?.l2_status === "boolean" &&
          dataDetailMoc.l2_status === false
        ) {
          const dataTemp = {
            id: dataDetailMoc.id,
            purchaseRequestNo: dataDetailMoc.purchase_request_no,
            description: dataDetailMoc.description,
            sapCode: dataDetailMoc.sap_code,
            L2Status: "Isn't Approved Yet",
          };
          ifNotApproved.push(dataTemp);
        }
      }

      if (ifNotApproved.length > 0) {
        return res.status(400).json({ message: ifNotApproved });
      }

      let poId: any = "";
      poId = await this.toolsApi.generateUUID();

      const poHeaderData = {
        uuid: poId,
        ship_name: getMocHeader?.ship_name,
        department: getMocHeader?.department,
        category: getMocHeader?.category,
        purchase_request_no: getMocHeader?.purchase_request_no,
        purchase_order_no: await this.toolsApi.generateUnique(),
        location: getMocHeader?.location,
      };
      await this.poService
        .create(poHeaderData)
        .then(async () => {
          for (const detailMocItem of detailMocItems) {
            const getMocItemDetail = await this.service.findOne(
              detailMocItem.id
            );
            const poItemData = {
              po_id: poId,
              description: getMocItemDetail?.description,
              quantity: getMocItemDetail?.quantity,
              unit: getMocItemDetail?.unit,
              shop_one_name: getMocItemDetail?.shop_one_name,
              shop_one_unit_price: getMocItemDetail?.shop_one_unit_price,
              shop_one_total_price: getMocItemDetail?.shop_one_total_price,
              shop_one_remarks: getMocItemDetail?.shop_one_remarks,
              shop_one_tax: getMocItemDetail?.shop_one_tax,
              shop_two_name: getMocItemDetail?.shop_two_name,
              shop_two_unit_price: getMocItemDetail?.shop_two_unit_price,
              shop_two_total_price: getMocItemDetail?.shop_two_total_price,
              shop_two_remarks: getMocItemDetail?.shop_two_remarks,
              shop_two_tax: getMocItemDetail?.shop_two_tax,
              shop_three_name: getMocItemDetail?.shop_three_name,
              shop_three_unit_price: getMocItemDetail?.shop_three_unit_price,
              shop_three_total_price: getMocItemDetail?.shop_three_total_price,
              shop_three_remarks: getMocItemDetail?.shop_three_remarks,
              shop_three_tax: getMocItemDetail?.shop_three_tax,
            };
            await this.poItemsService.create(poItemData).then(async () => {
              const dataApproved = {
                status: "Close",
              };
              await this.service.update(detailMocItem.id, dataApproved);
            });
          }
        })
        .then(async () => {
          const updateHeaderMoc = {
            status: "Close",
          };
          await this.mocService.update(idMoc, updateHeaderMoc);
        });

      res.status(200).json({ message: "Data Succesfully Moved" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }
}
