import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { ApprovedPoItemsService } from "../services/approvedPoItemsService";

export class ApprovedPoItemsController {
  private toolsApi: Tools;
  private service: ApprovedPoItemsService;

  constructor() {
    this.toolsApi = new Tools();
    this.service = new ApprovedPoItemsService();
  }

  public async getById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const aprPoItemsHeader = await this.service.findOne(id);
      if (!aprPoItemsHeader) {
        return res.status(204).json({ message: "Purchase Order Id not found" });
      }

      const data = this.toolsApi.snakeToCamel(aprPoItemsHeader.toJSON());

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }
}
