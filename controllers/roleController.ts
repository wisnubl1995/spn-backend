import { Request, Response } from "express";
import { RoleService } from "../services/roleService";
import { Tools } from "../utils/tools";

export class RoleController {
  private toolsApi: Tools;
  private service: RoleService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new RoleService();
  }

  public async create(req: Request, res: Response) {
    const { roleName } = req.body;
    try {
      const data = {
        uuid: await this.toolsApi.generateUUID(),
        role_name: roleName,
      };
      await this.service.create(data);
      res.status(201).json({ message: "Data Successfuly Created" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    const { page, pageSize } = req.query;
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    try {
      const roles = await this.service.fetch(pageNumber, pageSizeN);
      if (roles.length == 0) {
        return res.status(204).json();
      }
      const data = roles.map((roleData) => {
        const transformRole = this.toolsApi.snakeToCamel(roleData.toJSON());
        return transformRole;
      });

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async searchId(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const roles = await this.service.findOne(id);
      const data = await this.toolsApi.snakeToCamel(roles?.toJSON());
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async searchName(req: Request, res: Response) {
    const { params } = req.params;
    try {
      const roles = await this.service.findName(params);
      const data = roles.map((roleData) => {
        const transformRole = this.toolsApi.snakeToCamel(roleData.toJSON());
        return transformRole;
      });
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const { id, roleName } = req.body;
    try {
      await this.service.update(id, roleName);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id } = req.params;
    try {
      await this.service.delete(id);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Interna Server Error" });
    }
  }
}
