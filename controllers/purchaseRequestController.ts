import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { PurchaseRequestService } from "../services/purchaseRequestService";
import { PurchaseItemService } from "../services/purchaseItemService";

export class PurchaseRequestController {
  private toolsApi: Tools;
  private service: PurchaseRequestService;
  private purchaseItemService: PurchaseItemService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new PurchaseRequestService();
    this.purchaseItemService = new PurchaseItemService();
  }

  public async create(req: Request, res: Response) {
    const {
      shipName,
      department,
      category,
      purchaseRequestNo,
      requestedBy,
      location,
      purchaseItems,
      createdBy,
    } = req.body;
    try {
      const purchaseReqId = await this.toolsApi.generateUUID();
      const data = {
        uuid: purchaseReqId,
        ship_name: shipName,
        department: department,
        category: category,
        purchase_request_no: purchaseRequestNo,
        requested_by: requestedBy,
        location: location,
        created_by: createdBy,
      };
      await this.service.create(data);

      const purchaseItemsData = purchaseItems.map((item: any) => ({
        purchase_request_id: purchaseReqId,
        name: item.name,
        sap_code: item.sapCode,
        quantity_asked: item.quantityAsked,
        quantity_left: item.quantityLeft,
        quantity_approved: item.quantityApproved,
        unit: item.unit,
        description: item.description,
        additional_info: item.additionalInfo,
        image: item.image,
      }));

      await this.purchaseItemService.create(purchaseItemsData);
      res.status(201).json({ message: "Data Successfuly Created" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const purchaseRequest = await this.service.findOne(id);
      if (!purchaseRequest) {
        return res.status(204).json({ message: "Purchase request not found" });
      }

      const transformedPurchaseItems = purchaseRequest.purchaseItems.map(
        (purchaseItem) => this.toolsApi.snakeToCamel(purchaseItem.toJSON())
      );

      const data = this.toolsApi.snakeToCamel(purchaseRequest.toJSON());
      data.purchaseItems = transformedPurchaseItems;

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    const { page, pageSize } = req.query;
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    try {
      const purchaseRequests = await this.service.fetch(pageNumber, pageSizeN);
      if (purchaseRequests.length == 0) {
        return res.status(204).json();
      }
      const data = purchaseRequests.map((purchaseRequest) => {
        const transformedPurchaseItems = purchaseRequest.purchaseItems.map(
          (purhcaseItem) => {
            return this.toolsApi.snakeToCamel(purhcaseItem.toJSON());
          }
        );
        const transformedPurchaseRequest = this.toolsApi.snakeToCamel(
          purchaseRequest.toJSON()
        );
        transformedPurchaseRequest.purchaseItems = transformedPurchaseItems;

        return transformedPurchaseRequest;
      });
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async search(req: Request, res: Response) {
    const queryParams: { [key: string]: any } = req.query;
    try {
      const purchaseRequests = await this.service.find(queryParams);
      if (!purchaseRequests || purchaseRequests.length === 0) {
        return res.status(204).json({ message: "No purchase requests found" });
      }

      const data = purchaseRequests.map((purchaseRequest: any) => {
        const transformedPurchaseItems = purchaseRequest.purchaseItems.map(
          (purchaseItem: any) =>
            this.toolsApi.snakeToCamel(purchaseItem.toJSON())
        );

        const transformedPurchaseRequest = this.toolsApi.snakeToCamel(
          purchaseRequest.toJSON()
        );
        transformedPurchaseRequest.purchaseItems = transformedPurchaseItems;

        return transformedPurchaseRequest;
      });

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const {
      id,
      shipName,
      department,
      category,
      requestNo,
      quantity,
      sapCode,
      description,
      additionalInfo,
      requestedBy,
      location,
    } = req.body;
    try {
      const data = {
        ship_name: shipName,
        department: department,
        category: category,
        request_no: requestNo,
        quantity: quantity,
        sap_code: sapCode,
        description: description,
        additional_info: additionalInfo,
        requested_by: requestedBy,
        location: location,
      };
      await this.service.update(id, data);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id } = req.params;
    const idString = typeof id === "string" ? id : "default";
    try {
      const data = await this.service.delete(idString);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async updateStatusPrHeader(idPr: string) {
    const { count } = await this.purchaseItemService.count(idPr);
    if (Array.isArray(count) && count.length > 0) {
      const allOpen = count.every((item) => item.status === "Open");
      const allClose = count.every((item) => item.status === "Close");

      if (allOpen) {
        const data: any = {
          status: "Open",
        };
      } else if (allClose) {
        const data: any = {
          status: "Close",
        };
        return await this.service.update(idPr, data);
      } else {
        const data: any = {
          status: "On Progress",
        };
        return await this.service.update(idPr, data);
      }
    }
  }
}
