import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { CategoryService } from "../services/categoryService";

export class CategoryController {
  private toolsApi: Tools;
  private service: CategoryService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new CategoryService();
  }

  public async create(req: Request, res: Response) {
    const { name, desc } = req.body;
    try {
      const data = {
        uuid: await this.toolsApi.generateUUID(),
        name: name,
        desc: desc,
      };
      await this.service.create(data);
      res.status(201).json({ message: "Data Successfuly Created" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    try {
      const data = await this.service.fetch();
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async search(req: Request, res: Response) {
    const { params } = req.params;
    try {
      const data = await this.service.find({ key: params });
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const { id, name, desc } = req.body;
    try {
      const data = {
        name: name,
        desc: desc,
        location: location,
      };
      await this.service.update(id, data);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id } = req.params;
    try {
      await this.service.delete(id);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Interna Server Error" });
    }
  }
}
