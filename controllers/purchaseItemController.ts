import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { PurchaseItemService } from "../services/purchaseItemService";
import { MOCService } from "../services/mocService";
import { PurchaseRequestService } from "../services/purchaseRequestService";
import { MocItemService } from "../services/mocItemService";
import { PurchaseRequestController } from "./purchaseRequestController";

export class PurchaseItemController {
  private toolsApi: Tools;
  private service: PurchaseItemService;
  private PurchaseRequestService: PurchaseRequestService;
  private MOCService: MOCService;
  private MocItemService: MocItemService;
  private PurcahaseRequestController: PurchaseRequestController;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new PurchaseItemService();
    this.PurchaseRequestService = new PurchaseRequestService();
    this.MOCService = new MOCService();
    this.MocItemService = new MocItemService();
    this.PurcahaseRequestController = new PurchaseRequestController();
  }

  public async getById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const purchaseItem = await this.service.findOne(id);
      if (!purchaseItem) {
        return res.status(204).json({ message: "Purchase item not found" });
      }

      const data = this.toolsApi.snakeToCamel(purchaseItem.toJSON());
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const {
      id,
      name,
      unit,
      quantityAsked,
      quantityLeft,
      quantityApproved,
      sapCode,
      description,
      additionalInfo,
      image,
      status,
      isApproved,
      approvedBy,
      quantityApprovedBy,
    } = req.body;
    try {
      const data = {
        name: name,
        unit: unit,
        quantity_asked: quantityAsked,
        quantity_left: quantityLeft,
        quantity_approved: quantityApproved,
        sap_code: sapCode,
        description: description,
        additional_info: additionalInfo,
        image: image,
        is_approved: isApproved,
        status: status,
        approved_by: approvedBy,
        quantity_approved_by: quantityApprovedBy,
      };
      await this.service.update(id, data);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async moveItemToMOC(req: Request, res: Response) {
    const { idPr, prItemDetails } = req.body;
    try {
      const getPrHeader = await this.PurchaseRequestService.findOne(idPr);
      if (!getPrHeader?.uuid) {
        return res.status(404).json({ message: "Purchase Request Not Found" });
      }

      if (getPrHeader.status === "Close") {
        return res
          .status(404)
          .json({ message: "Purchase Request Already Closed" });
      }
      //validation
      for (const prItemDetail of prItemDetails) {
        const isApproved = await this.service.findOne(prItemDetail.id);
        if (isApproved?.quantity_approved == null) {
          return res
            .status(400)
            .json({ message: "Quantity Approved is still empty" });
        }
      }

      let mocId: any = "";

      mocId = await this.toolsApi.generateUUID();
      const mocHeaderData = {
        uuid: mocId,
        ship_name: getPrHeader?.ship_name,
        department: getPrHeader?.department,
        category: getPrHeader?.category,
        purchase_request_no: getPrHeader?.purchase_request_no,
        location: getPrHeader?.location,
      };
      await this.MOCService.create(mocHeaderData);

      let responseData: any = [];
      for (const prItemDetail of prItemDetails) {
        const dataApproved = {
          status: "Close",
          is_approved: true,
        };
        await this.service
          .update(prItemDetail.id, dataApproved)
          .then(async () => {
            const getPrItemDetail = await this.service.findOne(prItemDetail.id);
            const mocItemData = {
              moc_id: mocId,
              purchase_request_no: getPrHeader?.purchase_request_no,
              description: getPrItemDetail?.description,
              unit: getPrItemDetail?.unit,
              quantity: getPrItemDetail?.quantity_approved,
              sap_code: getPrItemDetail?.sap_code,
            };
            await this.MocItemService.create(mocItemData);
            const idPrItem = { id: prItemDetail.id, status: "Close" };
            responseData.push(idPrItem);
          });
      }
      await this.PurcahaseRequestController.updateStatusPrHeader(idPr);
      res
        .status(200)
        .json({ message: "Data succesfuly moved", data: responseData });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id } = req.params;
    const idString = typeof id === "string" ? id : "default";
    try {
      await this.service.delete(idString);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }
}
