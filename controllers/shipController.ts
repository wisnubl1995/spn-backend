import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { ShipService } from "../services/shipService";

export class ShipController {
  private toolsApi: Tools;
  private service: ShipService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new ShipService();
  }

  public async create(req: Request, res: Response) {
    const { name, shipCode, desc, image, location } = req.body;
    try {
      const data = {
        uuid: await this.toolsApi.generateUUID(),
        name: name,
        ship_code: shipCode,
        desc: desc,
        image: image,
        location: location,
      };
      await this.service.create(data);
      res.status(201).json({ message: "Data Successfuly Created" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    try {
      const ship = await this.service.fetch();
      res.status(200).json({ data: ship });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async search(req: Request, res: Response) {
    const { params } = req.params;
    try {
      const ship = await this.service.find({ key: params });
      res.status(200).json({ data: ship });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const { id, name, shipCode, desc, image, location } = req.body;
    try {
      const data = {
        name: name,
        ship_code: shipCode,
        desc: desc,
        image: image,
        location: location,
      };
      await this.service.update(id, data);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id } = req.params;
    try {
      await this.service.delete(id);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Interna Server Error" });
    }
  }
}
