import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { MOCService } from "../services/mocService";
import { MocItemService } from "../services/mocItemService";
import { PurchaseRequestService } from "../services/purchaseRequestService";

export class MocController {
  private toolsApi: Tools;
  private service: MOCService;
  private mocItems: MocItemService;
  private purchaseRequestService: PurchaseRequestService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new MOCService();
    this.mocItems = new MocItemService();
    this.purchaseRequestService = new PurchaseRequestService();
  }

  public async getAll(req: Request, res: Response) {
    const { page, pageSize } = req.query;
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    try {
      const mocDatas = await this.service.fetch(pageNumber, pageSizeN);
      if (mocDatas.length == 0) {
        return res.status(204).json();
      }
      const data = mocDatas.map((mocData) => {
        const transformedMocItems = mocData.mocItems.map((mocItem) => {
          return this.toolsApi.snakeToCamel(mocItem.toJSON());
        });
        const transformMoc = this.toolsApi.snakeToCamel(mocData.toJSON());
        transformMoc.mocItems = transformedMocItems;

        return transformMoc;
      });

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ mesage: "Internal Server Error" });
    }
  }

  public async getById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const mocHeader = await this.service.findOne(id);
      if (!mocHeader) {
        return res.status(204).json({ message: "Purchase request not found" });
      }

      const transformedPurchaseItems = mocHeader.mocItems.map((mocItem) =>
        this.toolsApi.snakeToCamel(mocItem.toJSON())
      );

      const data = this.toolsApi.snakeToCamel(mocHeader.toJSON());
      data.mocItems = transformedPurchaseItems;

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async updateStatusPrHeader(prNo: string, idMoc: string) {
    const status = await this.purchaseRequestService.getPrNo(prNo);
    const dataTmp = status[0];
    const result: any = dataTmp[0];
    return await this.service.update(idMoc, result);
  }
}
