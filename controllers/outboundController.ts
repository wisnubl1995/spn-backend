import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { OutboundService } from "../services/outboundService";
import { InventoryService } from "../services/inventoryService";
import { OutboundItemService } from "../services/outboundItemService";

export class OutboundController {
  private toolsApi: Tools;
  private service: OutboundService;
  private outboundItemService: OutboundItemService;
  private inventoryService: InventoryService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new OutboundService();
    this.inventoryService = new InventoryService();
    this.outboundItemService = new OutboundItemService();
  }

  public async create(req: Request, res: Response) {
    const { deliverTo, via, noResi, location, detailInventories } = req.body;
    try {
      let tmpOutboundError: any[] = [];
      let outboundItemsData: any[] = [];

      for (const detailInventory of detailInventories) {
        const getInventoryData = await this.inventoryService.findByName(
          detailInventory.shipName,
          detailInventory.productName,
          location
        );

        if (
          !getInventoryData ||
          Number(getInventoryData.count) === 0 ||
          Number(getInventoryData.count) === null
        ) {
          tmpOutboundError.push(
            `Ship ${detailInventory.shipName} or Product ${detailInventory.productName} Not Found or Insufficient Inventory`
          );
          continue;
        }

        const inventoryCount = Number(getInventoryData.count);
        const detailCount = Number(detailInventory.count);

        const updatedCount = inventoryCount - detailCount;

        await this.inventoryService.update(
          getInventoryData.uuid,
          { count: updatedCount.toString() },
          location
        );

        const outboundItemData = {
          uuid: await this.toolsApi.generateUUID(),
          product_name: getInventoryData.product_name,
          unit: getInventoryData.unit,
          count: detailCount,
          additionalInfo: detailInventory.additionalInfo,
          status: "Open",
        };

        outboundItemsData.push(outboundItemData);
      }

      if (tmpOutboundError.length > 0) {
        return res.status(400).json({ message: tmpOutboundError });
      }

      const outboundHeader = {
        uuid: await this.toolsApi.generateUUID(),
        deliver_to: deliverTo,
        via: via,
        no_resi: noResi,
        location: location,
      };

      await this.service.create(outboundHeader);
      await this.outboundItemService.create(outboundItemsData);
      res.status(200).json({ message: "Data Succesfully Created" });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    const { location, page, pageSize } = req.params;
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    try {
      const data = await this.service.fetch(location, pageNumber, pageSizeN);

      if (data.length === 0) {
        return res.status(204).json();
      }
      const result = data.map((item) => {
        const transfrom = this.toolsApi.snakeToCamel(item.toJSON());
        return transfrom;
      });
      res.status(200).json({ data: result });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getById(req: Request, res: Response) {
    const { id, location } = req.query;
    const locationString = typeof location === "string" ? location : "default";
    const idString = typeof id === "string" ? id : "default";
    try {
      const outboundData = await this.service.findOne(idString, locationString);
      if (!outboundData) {
        return res.status(204).json();
      }
      const data = this.toolsApi.snakeToCamel(outboundData.toJSON());
      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async search(req: Request, res: Response) {
    const { params, location, start, limit } = req.query;
    const paramsString = typeof params === "string" ? params : "default";
    const locationString = typeof location === "string" ? location : "default";
    const pageNumber = Number(start);
    const pageSizeN = Number(limit);
    try {
      const data = await this.service.find(
        paramsString,
        locationString,
        pageNumber,
        pageSizeN
      );

      const result = data.map((item) => {
        const transfrom = this.toolsApi.snakeToCamel(item.toJSON());
        return transfrom;
      });
      res.status(200).json({ data: result });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }
  public async update(req: Request, res: Response) {
    const { id, productName, location } = req.body;
    try {
      const data = {
        product_name: productName,
        location: location,
      };
      await this.service.update(id, data, location);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id, location } = req.query;
    const locationString = typeof location === "string" ? location : "default";
    const idString = typeof id === "string" ? id : "default";
    try {
      const data = await this.service.delete(idString, locationString);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Interna Server Error" });
    }
  }
}
