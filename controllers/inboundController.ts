import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { InboundService } from "../services/inboundService";
import { InventoryService } from "../services/inventoryService";

export class InboundController {
  private toolsApi: Tools;
  private service: InboundService;
  private inventoryService: InventoryService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new InboundService();
    this.inventoryService = new InventoryService();
  }

  public async create(req: Request, res: Response) {
    const { productName, count, location, shipName } = req.body;
    try {
      const data = {
        uuid: await this.toolsApi.generateUUID(),
        product_name: productName,
        count: count,
        location: location,
        ship_name: shipName,
      };
      await this.service.create(data);
      res.status(201).json({ message: "Data Successfuly Created" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    const { location, page, pageSize } = req.query;
    const locationString = typeof location === "string" ? location : "default";
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    try {
      const inboundData = await this.service.fetch(
        locationString,
        pageNumber,
        pageSizeN
      );
      if (inboundData.length == 0) {
        return res.status(204).json();
      }
      const data = inboundData.map((item) => {
        const transformInbound = this.toolsApi.snakeToCamel(item.toJSON());
        return transformInbound;
      });
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getById(req: Request, res: Response) {
    const { id, location } = req.query;
    const locationString = typeof location === "string" ? location : "default";
    const idString = typeof id === "string" ? id : "default";
    try {
      const inboundData = await this.service.findOne(idString, locationString);
      if (!inboundData) {
        return res.status(204).json();
      }
      const data = this.toolsApi.snakeToCamel(inboundData.toJSON());
      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async search(req: Request, res: Response) {
    const { params, location, page, pageSize } = req.query;
    const paramsString = typeof params === "string" ? params : "default";
    const locationString = typeof location === "string" ? location : "default";
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    try {
      const data = await this.service.find(
        paramsString,
        locationString,
        pageNumber,
        pageSizeN
      );

      const result = data.map((item) => {
        const transfrom = this.toolsApi.snakeToCamel(item.toJSON());
        return transfrom;
      });
      res.status(200).json({ data: result });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const { id, productName, location, count, status } = req.body;
    try {
      const data = {
        uuid: id,
        product_name: productName,
        location: location,
        count,
        status,
      };
      await this.service.update(id, data, location);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id, location } = req.query;
    const locationString = typeof location === "string" ? location : "default";
    const idString = typeof id === "string" ? id : "default";
    try {
      const data = await this.service.delete(idString, locationString);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async moveToInventory(req: Request, res: Response) {
    const { id, location } = req.body;
    const locationString = typeof location === "string" ? location : "default";
    const idString = typeof id === "string" ? id : "default";
    try {
      const getInboundData = await this.service.findOne(
        idString,
        locationString
      );
      if (!getInboundData) {
        return res.status(400).json({ message: "Bad Request" });
      }

      if (getInboundData.status === "Close") {
        return res.status(400).json({ message: "Data already moved" });
      }

      const getInventoryData = await this.inventoryService.findByName(
        getInboundData.ship_name,
        getInboundData.product_name,
        locationString
      );

      if (!getInventoryData) {
        const inventoryData = {
          uuid: await this.toolsApi.generateUUID(),
          product_name: getInboundData.product_name,
          ship_name: getInboundData.ship_name,
          count: getInboundData.count,
          status: "Available",
          unit: getInboundData.unit,
          location: locationString,
        };
        await this.inventoryService.create(inventoryData).then(async () => {
          const updateInboundData = {
            status: "Close",
          };
          await this.service.update(
            idString,
            updateInboundData,
            locationString
          );
        });
        return res.status(200).json({ message: "Data Succesfully Moved" });
      } else {
        let total;
        total = Number(getInboundData.count) + Number(getInventoryData.count);
        const inventoryData = {
          count: await total.toString(),
        };
        this.inventoryService
          .update(getInventoryData.uuid, inventoryData, locationString)
          .then(async () => {
            const updateInboundData = {
              status: "Close",
            };
            await this.service.update(
              idString,
              updateInboundData,
              locationString
            );
          });
        return res.status(200).json({ message: "Data Successfully Moved" });
      }
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }
}
