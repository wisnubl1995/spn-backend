import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { ModuleService } from "../services/moduleService";

export class ModuleController {
  private toolsApi: Tools;
  private service: ModuleService;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new ModuleService();
  }

  public async create(req: Request, res: Response) {
    const { accountId, moduleName, moduleRouter } = req.body;
    try {
      const data = {
        uuid: await this.toolsApi.generateUUID(),
        account_id: accountId,
        module_name: moduleName,
        module_router: moduleRouter,
      };
      await this.service.create(data);
      res.status(201).json({ message: "Data Successfuly Created" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    try {
      const modules = await this.service.fetch();
      const data = modules.map((module) => {
        const dataTransform = this.toolsApi.snakeToCamel(module.toJSON());
        return dataTransform;
      });
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const modules = await this.service.findOne(id);
      if (!modules) {
        return res.status(204).json({ message: "module not found" });
      }
      const data = this.toolsApi.snakeToCamel(modules.toJSON());
      res.status(200).json({ data: module });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async search(req: Request, res: Response) {
    const { params } = req.params;
    try {
      const module = await this.service.find({ key: params });
      res.status(200).json({ data: module });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const { id, accountId, moduleName } = req.body;
    try {
      const data = {
        module_name: moduleName,
        account_id: accountId,
      };
      await this.service.update(id, data);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id } = req.params;
    try {
      await this.service.delete(id);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Interna Server Error" });
    }
  }
}
