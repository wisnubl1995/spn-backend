import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { ApprovedPoService } from "../services/approvedPoService";
import { ApprovedPoItemsService } from "../services/approvedPoItemsService";
import { InboundService } from "../services/inboundService";

export class ApprovedPoController {
  private toolsApi: Tools;
  private service: ApprovedPoService;
  private aprPoItemsService: ApprovedPoItemsService;
  private inboundService: InboundService;

  constructor() {
    this.toolsApi = new Tools();
    this.service = new ApprovedPoService();
    this.aprPoItemsService = new ApprovedPoItemsService();
    this.inboundService = new InboundService();
  }

  public async getAll(req: Request, res: Response) {
    const { page, pageSize } = req.query;
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    try {
      const aprPoDatas = await this.service.fetch(pageNumber, pageSizeN);
      if (aprPoDatas.length == 0) {
        return res.status(204).json();
      }
      const data = aprPoDatas.map((aprPoData) => {
        const transformedMocItems = aprPoData.approvedPoItems.map(
          (aprPoItem) => {
            return this.toolsApi.snakeToCamel(aprPoItem.toJSON());
          }
        );
        const transformMoc = this.toolsApi.snakeToCamel(aprPoData.toJSON());
        transformMoc.approvedPoItems = transformedMocItems;

        return transformMoc;
      });

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ mesage: "Internal Server Error" });
    }
  }

  public async getById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const aprPoHeader = await this.service.findOne(id);
      if (!aprPoHeader) {
        return res.status(204).json({ message: "Purchase Order Id not found" });
      }

      const transformedPurchaseItems = aprPoHeader.approvedPoItems.map(
        (aprPoItem) => this.toolsApi.snakeToCamel(aprPoItem.toJSON())
      );

      const data = this.toolsApi.snakeToCamel(aprPoHeader.toJSON());
      data.approvedPoItems = transformedPurchaseItems;

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async moveAprPoToInbound(req: Request, res: Response) {
    const { idPo, location, detailAprPoItems } = req.body;
    try {
      const getHeaderAprPo = await this.service.findOne(idPo);
      const updatePoData = {
        status: "Close",
      };
      if (!getHeaderAprPo) {
        return res
          .status(500)
          .json({ message: "Unable to retrieve header purchase order " });
      }
      for (const detailPoItem of detailAprPoItems) {
        const getAprPoDatas = await this.aprPoItemsService.findOne(
          detailPoItem.id
        );

        if (!getAprPoDatas) {
          return res
            .status(500)
            .json({ message: "Unable to retrieve data detail purchase order" });
        }
        const dataInbound = {
          uuid: await this.toolsApi.generateUUID(),
          product_name: getAprPoDatas.description,
          count: getAprPoDatas.quantity,
          ship_name: getHeaderAprPo.ship_name,
          order_number: getHeaderAprPo.order_number,
          location: location,
          unit: detailPoItem.unit,
          status: "Pending",
        };
        await this.inboundService.create(dataInbound).then(async () => {
          await this.aprPoItemsService.update(detailPoItem.id, updatePoData);
        });
      }
      await this.service.update(getHeaderAprPo.uuid, updatePoData);
      res.status(200).json({ message: "Data Succesfuly Move" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }
}
