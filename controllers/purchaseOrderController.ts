import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { PurchaseOrderService } from "../services/purchaseOrderService";
import { ApprovedPoService } from "../services/approvedPoService";
import { ApprovedPoItemsService } from "../services/approvedPoItemsService";
import { PurchaseOrderItemService } from "../services/purchaseOrderItemService";

export class PurchaseOrderController {
  private toolsApi: Tools;
  private service: PurchaseOrderService;
  private purchaseOrderItemService: PurchaseOrderItemService;
  private aprPoService: ApprovedPoService;
  private aprPoItemsService: ApprovedPoItemsService;

  constructor() {
    this.toolsApi = new Tools();
    this.service = new PurchaseOrderService();
    this.purchaseOrderItemService = new PurchaseOrderItemService();
    this.aprPoService = new ApprovedPoService();
    this.aprPoItemsService = new ApprovedPoItemsService();
  }

  public async getAll(req: Request, res: Response) {
    const { page, pageSize } = req.query;
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    try {
      const poDatas = await this.service.fetch(pageNumber, pageSizeN);
      if (poDatas.length == 0) {
        return res.status(204).json();
      }
      const data = poDatas.map((poData) => {
        const transformedMocItems = poData.purchaseOrderItem.map((poItem) => {
          return this.toolsApi.snakeToCamel(poItem.toJSON());
        });
        const transformMoc = this.toolsApi.snakeToCamel(poData.toJSON());
        transformMoc.purchaseOrderItem = transformedMocItems;

        return transformMoc;
      });

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ mesage: "Internal Server Error" });
    }
  }

  public async movePoToAprPo(req: Request, res: Response) {
    const {
      idPo,
      billingTo,
      quotationNumber,
      project,
      orderNumber,
      orderDate,
      detailPoItems,
    } = req.body;
    try {
      const getHeaderPo = await this.service.findOne(idPo);
      if (!getHeaderPo) {
        return res.status(404).json({ message: "Purchase Order Id Not Found" });
      }

      if (getHeaderPo.status === "Close") {
        return res.status(404).json({ message: "Purchase Order is Closed" });
      }
      let uuid = await this.toolsApi.generateUUID();
      const dataHeaderAprPo = {
        uuid: uuid,
        billing_to: billingTo,
        quotation_number: quotationNumber,
        department: getHeaderPo?.department,
        ship_name: getHeaderPo?.ship_name,
        purchase_request_no: getHeaderPo?.purchase_request_no,
        project: project,
        order_number: orderNumber,
        order_date: orderDate,
      };
      await this.aprPoService.create(dataHeaderAprPo);
      for (const detailPoItem of detailPoItems) {
        const aprPoItemData = {
          apr_po_id: uuid,
          description: detailPoItem.description,
          quantity: detailPoItem.quantity,
          unit: detailPoItem.unit,
          unit_price: detailPoItem.unitPrice,
          amount: detailPoItem.amount,
          po_no: detailPoItem.poNo,
          tax: detailPoItem.tax,
          discount: detailPoItem.discount,
          ship_name: getHeaderPo.ship_name,
        };
        await this.aprPoItemsService.create(aprPoItemData);
        const status = {
          status: "Close",
        };
        await this.purchaseOrderItemService.update(detailPoItem.id, status);
      }
      const updateHeaderPO = {
        status: "Close",
      };
      await this.service.update(idPo, updateHeaderPO);
      res.status(200).json({ message: "Data Successfuly Move" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }
  public async getById(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const poHeader = await this.service.findOne(id);
      if (!poHeader) {
        return res.status(204).json({ message: "Purchase Order Id not found" });
      }

      const transformedPurchaseItems = poHeader.purchaseOrderItem.map(
        (poItem) => this.toolsApi.snakeToCamel(poItem.toJSON())
      );

      const data = this.toolsApi.snakeToCamel(poHeader.toJSON());
      data.purchaseOrderItem = transformedPurchaseItems;

      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }
}
