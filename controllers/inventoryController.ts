import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { InventoryService } from "../services/inventoryService";
import { LogLevel, Logging } from "../middleware/logger";
import { v4 } from "uuid";

export class InventoryController {
  private toolsApi: Tools;
  private service: InventoryService;
  private logger: Logging;
  constructor() {
    this.toolsApi = new Tools();
    this.service = new InventoryService();
    this.logger = new Logging();
  }

  public async create(req: Request, res: Response) {
    const { name, stock, weight, desc, location, threshold } = req.body;
    const { files }: any = req;
    const domain = req.headers.host;
    try {
      let imageUrl;
      if (files && files.length > 0) {
        imageUrl = `http://${domain}${req.baseUrl}/${files[0].filename}`;
      }
      const data = {
        uuid: await this.toolsApi.generateUUID(),
        name: name,
        stock: stock,
        weight: weight,
        desc: desc,
        image: imageUrl,
        location: location,
        threshold: threshold,
      };
      await this.service.create(data);
      res.status(201).json({ message: "Data Successfuly Created" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    const { location, page, pageSize } = req.query;
    const pageNumber = Number(page);
    const pageSizeN = Number(pageSize);
    const locationString = typeof location === "string" ? location : "default";
    try {
      const data = await this.service.fetch(
        locationString,
        pageNumber,
        pageSizeN
      );
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async search(req: Request, res: Response) {
    const { params, location } = req.query;
    const locationString = typeof location === "string" ? location : "default";
    try {
      const data = await this.service.find({ key: params }, locationString);
      res.status(200).json({ data: data });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const { id, name, stock, weight, desc, image, location, threshold } =
      req.body;
    try {
      const data = {
        name: name,
        stock: stock,
        weight: weight,
        desc: desc,
        image: image,
        location: location,
        threshold: threshold,
      };
      await this.service.update(id, data, location);
      res.status(200).json({ message: "Data Successfully Updated" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async delete(req: Request, res: Response) {
    const { id, location } = req.query;
    const locationString = typeof location === "string" ? location : "default";
    const idString = typeof id === "string" ? id : "default";
    try {
      const data = await this.service.delete(idString, locationString);
      res.status(200).json({ message: "Data Successfully Deleted" });
    } catch (error) {
      return res.status(500).json({ message: "Interna Server Error" });
    }
  }
}
