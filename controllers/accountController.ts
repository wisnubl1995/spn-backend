import { Request, Response } from "express";
import { Tools } from "../utils/tools";
import { AccountService } from "../services/accountService";

export class AccountController {
  private toolsApi: Tools;
  private service: AccountService;

  constructor() {
    this.toolsApi = new Tools();
    this.service = new AccountService();
  }

  public async create(req: Request, res: Response) {
    const {
      username,
      password,
      roleId,
      name,
      location,
      status,
      idEmployee,
      division,
      ltree,
    } = req.body;
    try {
      const data = {
        uuid: await this.toolsApi.generateUUID(),
        username: username,
        name: name,
        password: await this.toolsApi.hashPassword(password),
        role_id: roleId,
        location: location,
        status: status,
        id_employee: idEmployee,
        division: division,
        ltree: ltree,
      };

      await this.service.create(data);
      return res.status(201).json({ message: "Data created successfully" });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async login(req: Request, res: Response) {
    const { username, password } = req.body;
    try {
      const account = await this.service.login(username, password);
      if (account === false) {
        return res.status(401).json({ message: "invalid password" });
      }
      if (account === null) {
        return res.status(204).json({ message: "account not found" });
      }
      res
        .status(200)
        .json({ message: "account succesfully login", data: account });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async getAll(req: Request, res: Response) {
    try {
      const account = await this.service.fetch();
      return res.status(200).json({ data: account });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async search(req: Request, res: Response) {
    const { params } = req.params;
    try {
      const account = await this.service.find({ key: params });
      res.status(200).json({ data: account });
    } catch (error) {
      return res.status(500).json({ message: "Internal Server Error" });
    }
  }

  public async update(req: Request, res: Response) {
    const {
      id,
      name,
      status,
      username,
      password,
      location,
      roleId,
      idEmployee,
      division,
      ltree,
    } = req.body;
    try {
      const data = {
        name: name,
        status: status,
        username: username,
        password: password,
        location: location,
        role_id: roleId,
        id_employee: idEmployee,
        division: division,
        ltree: ltree,
      };
      await this.service.update(id, data);
      res.status(200).json({ message: "Account updated successfully" });
    } catch (error) {}
  }
}
