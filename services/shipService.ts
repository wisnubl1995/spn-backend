import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Ship, ShipModel } from "../models/ship";

export class ShipService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: ShipModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const ship = await Ship.create(data);
      return ship;
    });
    return result;
  }

  public async fetch() {
    const ship = await Ship.findAll();
    return ship;
  }

  public async find(params: { [key: string]: any }) {
    const ship = await Ship.findOne({ where: params });
    return ship;
  }

  public async update(id: string, data: ShipModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const ship = await Ship.update(data, { where: { uuid: id } });
      return ship;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const ship = await Ship.destroy({
        where: { uuid: id },
      });
      return ship;
    });
    return result;
  }
}
