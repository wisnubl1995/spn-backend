import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { PurchaseItem, PurchaseItemModel } from "../models/purchaseItems";

export class PurchaseItemService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: PurchaseItemModel[]) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseItem = await PurchaseItem.bulkCreate(data);
      return purchaseItem;
    });
    return result;
  }

  public async fetch(location: string, page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const purchaseItem = await PurchaseItem.findAll({
      limit: pageSize,
      offset: offset,
    });
    return purchaseItem;
  }

  public async findOne(id: string) {
    const purchaseItem = await PurchaseItem.findOne({ where: { uuid: id } });
    return purchaseItem;
  }

  public async count(params: string) {
    const purchaseItem = await PurchaseItem.findAndCountAll({
      attributes: ["status"],
      group: ["status"],
    });
    return purchaseItem;
  }

  public async update(id: string, data: PurchaseItemModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseItem = await PurchaseItem.update(data, {
        where: [{ uuid: id }],
      });
      return purchaseItem;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseItem = await PurchaseItem.destroy({
        where: [{ uuid: id }],
      });
      return purchaseItem;
    });
    return result;
  }
}
