import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Op, where } from "sequelize";
import {
  MatrixOfComparison,
  MatrixOfComparisonModel,
} from "../models/matrixOfComparison";
import { mocItems } from "../models/mocItems";

export class MOCService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: MatrixOfComparisonModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseRequest = await MatrixOfComparison.create(data);
      return purchaseRequest;
    });
    return result;
  }

  public async fetch(page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const purchaseRequest = await MatrixOfComparison.findAll({
      limit: pageSize,
      offset: offset,
      include: [mocItems],
    });
    return purchaseRequest;
  }

  public async findOne(id: string) {
    const result = await MatrixOfComparison.findOne({
      where: { uuid: id },
      include: [mocItems],
    });
    return result;
  }

  public async getPrNo(prNo: string) {
    const result = await this.sequelize.query(
      `SELECT * FROM spndb.matrix_of_comparison WHERE purchase_request_no = '${prNo}'`
    );
    return result;
  }

  public async update(id: string, data: MatrixOfComparisonModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseItem = await MatrixOfComparison.update(data, {
        where: [{ uuid: id }],
      });
      return purchaseItem;
    });
    return result;
  }
}
