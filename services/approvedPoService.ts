import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Op, where } from "sequelize";
import { ApprovedPo, ApprovedPoModel } from "../models/approvedPo";
import { ApprovedPoItems } from "../models/approvedPoItems";

export class ApprovedPoService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: ApprovedPoModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const approvedPo = await ApprovedPo.create(data);
      return approvedPo;
    });
    return result;
  }

  public async fetch(page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const approvedPo = await ApprovedPo.findAll({
      limit: pageSize,
      offset: offset,
      include: [ApprovedPoItems],
    });
    return approvedPo;
  }

  public async findOne(id: string) {
    const approvedPo = await ApprovedPo.findOne({
      where: { uuid: id },
      include: [ApprovedPoItems],
    });
    return approvedPo;
  }

  public async update(id: string, data: any) {
    const approvedPo = await ApprovedPo.update(data, { where: { uuid: id } });
    return approvedPo;
  }
}
