import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import {
  PurchaseRequest,
  PurchaseRequestModel,
} from "../models/purchaseRequest";
import { PurchaseItem } from "../models/purchaseItems";
import { Op, where } from "sequelize";

export class PurchaseRequestService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: PurchaseRequestModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseRequest = await PurchaseRequest.create(data);
      return purchaseRequest;
    });
    return result;
  }

  public async fetch(page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const purchaseRequest = await PurchaseRequest.findAll({
      limit: pageSize,
      offset: offset,
      include: [PurchaseItem],
    });
    return purchaseRequest;
  }

  public async findOne(id: string) {
    try {
      const purchaseRequest = await PurchaseRequest.findOne({
        where: { uuid: id },
        include: [PurchaseItem],
      });
      return purchaseRequest;
    } catch (error) {}
  }

  public async find(params: {
    [key: string]: any;
  }): Promise<PurchaseRequestModel[]> {
    const whereClause: { [key: string]: any } = {};
    for (const key in params) {
      if (typeof params[key] === "string") {
        whereClause[key] = { [Op.like]: `%${params[key]}%` };
      } else {
        whereClause[key] = params[key];
      }
    }
    const purchaseRequest = await PurchaseRequest.findAll({
      where: whereClause,
      include: [PurchaseItem],
    });
    return purchaseRequest;
  }

  public async update(id: string, data: PurchaseRequestModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseRequest = await PurchaseRequest.update(data, {
        where: [{ uuid: id }],
      });
      return purchaseRequest;
    });
    return result;
  }

  public async getPrNo(prNo: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseRequest = await this.sequelize.query(
        `SELECT status FROM "spndb"."purchase_request" WHERE ("purchase_request_no" = '${prNo}')`
      );
      return purchaseRequest;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseRequest = await this.sequelize
        .query(
          `DELETE FROM "spndb"."purchase_item" WHERE ("purchase_request_id" = '${id}')`
        )
        .then(async () => {
          PurchaseRequest.destroy({
            where: [{ uuid: id }],
            transaction: t,
          });
        });
      return purchaseRequest;
    });
    return result;
  }
}
