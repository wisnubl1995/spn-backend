import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { OutboundItems, OutboundItemsModel } from "../models/outboundItems";

export class OutboundItemService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: OutboundItemsModel[]) {
    const result = await this.sequelize.transaction(async (t) => {
      const outboundItem = await OutboundItems.bulkCreate(data);
      return outboundItem;
    });
    return result;
  }

  public async update(id: string, data: any) {
    const result = await this.sequelize.transaction(async (t) => {
      const OutboundItemsData = await OutboundItems.update(data, {
        where: [{ uuid: id }],
      });
      return OutboundItemsData;
    });
    return result;
  }

  public async findOne(id: string) {
    const result = await OutboundItems.findOne({
      where: { uuid: id },
    });

    return result;
  }
}
