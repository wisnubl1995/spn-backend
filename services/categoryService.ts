import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Category, CategoryModel } from "../models/category";

export class CategoryService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: CategoryModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const category = await Category.create(data);
      return category;
    });
    return result;
  }

  public async fetch() {
    const category = await Category.findAll();
    return category;
  }

  public async find(params: { [key: string]: any }) {
    const category = await Category.findOne({ where: params });
    return category;
  }

  public async update(id: string, data: CategoryModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const category = await Category.update(data, { where: { uuid: id } });
      return category;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const category = await Category.destroy({
        where: { uuid: id },
      });
      return category;
    });
    return result;
  }
}
