import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Unit, UnitModel } from "../models/unit";

export class UnitService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: UnitModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const unit = await Unit.create(data);
      return unit;
    });
    return result;
  }

  public async fetch() {
    const unit = await Unit.findAll();
    return unit;
  }

  public async find(params: { [key: string]: any }) {
    const unit = await Unit.findOne({ where: params });
    return unit;
  }

  public async update(id: string, data: UnitModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const unit = await Unit.update(data, { where: { uuid: id } });
      return unit;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const unit = await Unit.destroy({
        where: { uuid: id },
      });
      return unit;
    });
    return result;
  }
}
