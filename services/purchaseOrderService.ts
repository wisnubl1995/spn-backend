import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Op, where } from "sequelize";
import { PurchaseOrder, PurchaseOrderModel } from "../models/purchaseOrder";
import { PurchaseOrderItem } from "../models/poItems";

export class PurchaseOrderService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: PurchaseOrderModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseOrder = await PurchaseOrder.create(data);
      return purchaseOrder;
    });
    return result;
  }

  public async fetch(page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const purchaseOrder = await PurchaseOrder.findAll({
      limit: pageSize,
      offset: offset,
      include: [PurchaseOrderItem],
    });
    return purchaseOrder;
  }

  public async findOne(id: string) {
    const purchaseOrder = await PurchaseOrder.findOne({
      where: { uuid: id },
      include: [PurchaseOrderItem],
    });
    return purchaseOrder;
  }

  public async getPrNo(prNo: string) {
    const result = await this.sequelize.query(
      `SELECT * FROM spndb.purchase_order WHERE purchase_request_no = '${prNo}'`
    );
    return result;
  }

  public async update(id: string, data: PurchaseOrderModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseRequest = await PurchaseOrder.update(data, {
        where: [{ uuid: id }],
      });
      return purchaseRequest;
    });
    return result;
  }
}
