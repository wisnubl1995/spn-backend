import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { MOCItemsModel, mocItems } from "../models/mocItems";

export class MocItemService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: MOCItemsModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const mocItem = await mocItems.create(data);
      return mocItem;
    });
    return result;
  }

  public async update(id: string, data: any) {
    const result = await this.sequelize.transaction(async (t) => {
      const mocItemsData = await mocItems.update(data, {
        where: [{ uuid: id }],
      });
      return mocItemsData;
    });
    return result;
  }

  public async findOne(id: string) {
    const result = await mocItems.findOne({
      where: { uuid: id },
    });

    return result;
  }

  public async count(params: string) {
    const purchaseItem = await mocItems.findAndCountAll({
      attributes: ["status"],
      group: ["status"],
    });
    return purchaseItem;
  }
}
