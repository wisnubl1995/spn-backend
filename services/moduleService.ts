import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Module, ModuleModel } from "../models/module";

export class ModuleService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: ModuleModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const module = await Module.create(data);
      return module;
    });
    return result;
  }

  public async fetch() {
    const module = await Module.findAll();
    return module;
  }

  public async findOne(id: string) {
    const module = await Module.findOne({ where: { uuid: id } });
    return module;
  }

  public async find(params: { [key: string]: any }) {
    const module = await Module.findOne({ where: params });
    return module;
  }

  public async update(id: string, data: any) {
    const result = await this.sequelize.transaction(async (t) => {
      const module = await Module.update(data, { where: { uuid: id } });
      return module;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const module = await Module.destroy({
        where: { uuid: id },
      });
      return module;
    });
    return result;
  }
}
