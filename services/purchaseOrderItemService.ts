import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { PurchaseOrderItem, PurchaseOrderItemModel } from "../models/poItems";

export class PurchaseOrderItemService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: PurchaseOrderItemModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseOrderItem = await PurchaseOrderItem.create(data);
      return purchaseOrderItem;
    });
    return result;
  }

  public async fetch(location: string, page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const purchaseOrderItem = await PurchaseOrderItem.findAll({
      limit: pageSize,
      offset: offset,
    });
    return purchaseOrderItem;
  }

  public async findOne(id: string) {
    const purchaseOrderItem = await PurchaseOrderItem.findOne({
      where: { uuid: id },
    });
    return purchaseOrderItem;
  }

  public async count(idPo: string) {
    const purchaseOrderItem = await PurchaseOrderItem.findAndCountAll({
      where: { uuid: idPo },
      attributes: ["status"],
      group: ["status"],
    });
    return purchaseOrderItem;
  }

  public async update(id: string, data: PurchaseOrderItemModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseOrderItem = await PurchaseOrderItem.update(data, {
        where: [{ uuid: id }],
      });
      return purchaseOrderItem;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const purchaseOrderItem = await PurchaseOrderItem.destroy({
        where: [{ uuid: id }],
      });
      return purchaseOrderItem;
    });
    return result;
  }
}
