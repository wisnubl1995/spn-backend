import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import {
  ApprovedPoItems,
  ApprovedPoItemsModel,
} from "../models/approvedPoItems";

export class ApprovedPoItemsService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: ApprovedPoItemsModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const approvedPoItems = await ApprovedPoItems.create(data);
      return approvedPoItems;
    });
    return result;
  }

  public async fetch(page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const approvedPoItems = await ApprovedPoItems.findAll({
      limit: pageSize,
      offset: offset,
    });
    return approvedPoItems;
  }

  public async findOne(id: string) {
    const approvedPoItems = await ApprovedPoItems.findOne({
      where: { uuid: id },
    });
    return approvedPoItems;
  }

  public async count(params: string) {
    const approvedPoItems = await ApprovedPoItems.findAndCountAll({
      attributes: ["status"],
      group: ["status"],
    });
    return approvedPoItems;
  }

  public async update(id: string, data: ApprovedPoItemsModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const approvedPoItems = await ApprovedPoItems.update(data, {
        where: [{ uuid: id }],
      });
      return approvedPoItems;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const approvedPoItems = await ApprovedPoItems.destroy({
        where: [{ uuid: id }],
      });
      return approvedPoItems;
    });
    return result;
  }
}
