import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { DynamicTableName, Outbound, OutboundModel } from "../models/outbound";
import { OutboundItems } from "../models/outboundItems";

export class OutboundService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: OutboundModel) {
    const location = data.location || "";
    const DynamicOutboundModel = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await DynamicOutboundModel.sync();

    const result = await this.sequelize.transaction(async (t) => {
      const dynamicModel = DynamicOutboundModel.build(data);
      const savedData = await dynamicModel.save({ transaction: t });
      return savedData;
    });
    return result;
  }

  public async fetch(location: string, page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await dynamicTableName.sync();

    const inbound = await dynamicTableName.findAll({
      limit: pageSize,
      offset: offset,
      include: [OutboundItems],
    });
    return inbound;
  }

  public async findOne(id: string, location: string) {
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await dynamicTableName.sync();

    const inbound = await dynamicTableName.findOne({ where: { uuid: id } });
    return inbound;
  }

  public async find(
    params: string,
    location: string,
    page: number,
    pageSize: number
  ) {
    const offset = (page - 1) * pageSize;
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await dynamicTableName.sync();
    const modelAttributes = Object.keys(Outbound.getAttributes());
    const whereConditions: string[] = modelAttributes.map((attribute) => {
      return `${attribute} LIKE '%${params}%'`;
    });
    const whereClause = whereConditions.join(" OR ");

    const inbound = await dynamicTableName.findAll({
      where: this.sequelize.literal(`(${whereClause})`),
      limit: pageSize,
      offset: offset,
    });
    return inbound;
  }

  public async update(id: string, data: OutboundModel, location: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const dynamicTableName = DynamicTableName.initialize(
        this.sequelize,
        location
      );
      const inbound = await dynamicTableName.update(data, {
        where: { uuid: id },
        transaction: t,
      });
      return inbound;
    });
    return result;
  }

  public async delete(id: string, location: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const dynamicTableName = DynamicTableName.initialize(
        this.sequelize,
        location
      );
      const inbound = await dynamicTableName.destroy({
        where: { uuid: id },
        transaction: t,
      });
      return inbound;
    });
    return result;
  }
}
