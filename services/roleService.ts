import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Role, RoleModel } from "../models/role";
import { Op } from "sequelize";

export class RoleService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: RoleModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const role = await Role.create(data);
      return role;
    });
    return result;
  }

  public async fetch(page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const roles = await Role.findAll({ limit: pageSize, offset: offset });
    return roles;
  }

  public async findOne(params: string) {
    const role = await Role.findOne({ where: { uuid: params } });
    return role;
  }

  public async findName(params: string) {
    const role = await Role.findAll({
      where: { role_name: { [Op.like]: `%${params}%` } },
    });
    return role;
  }

  public async update(id: string, roleName: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const role = await Role.update(
        { role_name: roleName },
        { where: { uuid: id } }
      );
      return role;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const role = await Role.destroy({
        where: { uuid: id },
      });
      return role;
    });
    return result;
  }
}
