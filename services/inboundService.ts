import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { DynamicTableName, Inbound, InboundModel } from "../models/inbound";

export class InboundService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: InboundModel) {
    const location = data.location || "";
    const DynamicInboundModel = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await DynamicInboundModel.sync();

    const result = await this.sequelize.transaction(async (t) => {
      const dynamicModel = DynamicInboundModel.build(data);
      const savedData = await dynamicModel.save({ transaction: t });
      return savedData;
    });
    return result;
  }

  public async fetch(location: string, page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await dynamicTableName.sync();

    const inbound = await dynamicTableName.findAll({
      limit: pageSize,
      offset: offset,
    });
    return inbound;
  }

  public async findOne(id: string, location: string) {
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await dynamicTableName.sync();

    const inbound = await dynamicTableName.findOne({ where: { uuid: id } });
    return inbound;
  }

  public async findByName(
    shipName: string,
    location: string,
    productName: string
  ) {
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await dynamicTableName.sync();

    const inbound = await dynamicTableName.findOne({
      where: { ship_name: shipName, product_name: productName },
    });
    return inbound;
  }

  public async find(
    params: string,
    location: string,
    page: number,
    pageSize: number
  ) {
    const offset = (page - 1) * pageSize;
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await dynamicTableName.sync();
    const modelAttributes = Object.keys(Inbound.getAttributes());
    const whereConditions: string[] = modelAttributes.map((attribute) => {
      return `${attribute} LIKE '%${params}%'`;
    });
    const whereClause = whereConditions.join(" OR ");

    const inbound = await dynamicTableName.findAll({
      where: this.sequelize.literal(`(${whereClause})`),
      limit: pageSize,
      offset: offset,
    });
    return inbound;
  }

  public async update(id: string, data: InboundModel, location: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const dynamicTableName = DynamicTableName.initialize(
        this.sequelize,
        location
      );
      const inbound = await dynamicTableName.update(data, {
        where: { uuid: id },
        transaction: t,
      });
      return inbound;
    });
    return result;
  }

  public async delete(id: string, location: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const dynamicTableName = DynamicTableName.initialize(
        this.sequelize,
        location
      );
      const inbound = await dynamicTableName.destroy({
        where: { uuid: id },
        transaction: t,
      });
      return inbound;
    });
    return result;
  }
}
