import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { Department, DepartmentModel } from "../models/department";

export class DepartmentService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: DepartmentModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const department = await Department.create(data);
      return department;
    });
    return result;
  }

  public async fetch() {
    const department = await Department.findAll();
    return department;
  }

  public async find(params: { [key: string]: any }) {
    const department = await Department.findOne({ where: params });
    return department;
  }

  public async update(id: string, data: DepartmentModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const department = await Department.update(data, { where: { uuid: id } });
      return department;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const department = await Department.destroy({
        where: { uuid: id },
      });
      return department;
    });
    return result;
  }
}
