import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import { SapCode, SapCodeModel } from "../models/sapCode";

export class SapCodeService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: SapCodeModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const sapCode = await SapCode.create(data);
      return sapCode;
    });
    return result;
  }

  public async fetch() {
    const sapCode = await SapCode.findAll();
    return sapCode;
  }

  public async find(params: { [key: string]: any }) {
    const sapCode = await SapCode.findOne({ where: params });
    return sapCode;
  }

  public async update(id: string, data: SapCodeModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const sapCode = await SapCode.update(data, { where: { uuid: id } });
      return sapCode;
    });
    return result;
  }

  public async delete(id: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const sapCode = await SapCode.destroy({
        where: { uuid: id },
      });
      return sapCode;
    });
    return result;
  }
}
