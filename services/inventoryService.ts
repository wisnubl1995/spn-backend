import { Sequelize } from "sequelize-typescript";
import databaseInstance from "../config/database";
import {
  DynamicTableName,
  Inventory,
  InventoryModel,
} from "../models/inventory";

export class InventoryService {
  public sequelize: Sequelize;

  constructor() {
    this.sequelize = databaseInstance.getSequelize();
  }

  public async create(data: InventoryModel) {
    const location = data.location || "";
    const DynamicInventoryModel = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await DynamicInventoryModel.sync();

    const result = await this.sequelize.transaction(async (t) => {
      const dynamicModel = DynamicInventoryModel.build(data);
      const savedData = await dynamicModel.save({ transaction: t });
      return savedData;
    });
    return result;
  }

  public async fetch(location: string, page: number, pageSize: number) {
    const offset = (page - 1) * pageSize;
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    console.log(location, page, pageSize);
    await dynamicTableName.sync();

    const inventory = await dynamicTableName.findAll({
      limit: pageSize,
      offset: offset,
    });
    return inventory;
  }

  public async findByName(
    shipName: string,
    productName: string,
    location: string
  ) {
    const dynamicTableName = DynamicTableName.initialize(
      this.sequelize,
      location
    );

    await dynamicTableName.sync();

    const inventory = await dynamicTableName.findOne({
      where: { ship_name: shipName, product_name: productName },
    });
    return inventory;
  }

  public async find(params: { [key: string]: any }, location: string) {
    const inventory = await Inventory.findOne({
      where: { ...params, location: location },
    });
    return inventory;
  }

  public async update(id: string, data: InventoryModel, location: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const dynamicTableName = DynamicTableName.initialize(
        this.sequelize,
        location
      );
      const inventory = await dynamicTableName.update(data, {
        where: { uuid: id },
        transaction: t,
      });
      return inventory;
    });
    return result;
  }

  public async delete(id: string, location: string) {
    const result = await this.sequelize.transaction(async (t) => {
      const dynamicTableName = DynamicTableName.initialize(
        this.sequelize,
        location
      );
      const inventory = await dynamicTableName.destroy({
        where: { uuid: id },
        transaction: t,
      });
      return inventory;
    });
    return result;
  }
}
