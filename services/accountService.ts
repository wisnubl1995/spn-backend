import { Sequelize } from "sequelize-typescript";
import { Account, AccountModel } from "../models/account";
import databaseInstance from "../config/database";
import { Tools } from "../utils/tools";

export class AccountService {
  public sequelize: Sequelize;
  private toolsApi: Tools;
  constructor() {
    this.sequelize = databaseInstance.getSequelize();
    this.toolsApi = new Tools();
  }

  public async create(data: AccountModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const account = await Account.create(data);
      return account;
    });
    return result;
  }

  public async login(username: any, password: any) {
    if (!username) return null;
    const account = await Account.findOne({
      where: { username: username, status: 1 },
    });
    if (!this.toolsApi.comparePassword(account?.password ?? "", password)) {
      return false;
    }
    return account;
  }

  public async fetch() {
    const account = await Account.findAll({
      attributes: {
        exclude: ["password"],
      },
    });
    return account;
  }

  public async find(params: { [key: string]: any }) {
    const account = await Account.findOne({ where: params });
    return account;
  }

  public async update(id: string, data: AccountModel) {
    const result = await this.sequelize.transaction(async (t) => {
      const account = await Account.update(data, { where: { uuid: id } });
      return account;
    });
    return result;
  }
}
