import express, { Request, Response, NextFunction } from "express";
import createError from "http-errors";

class NotFoundHandler {
  static handle(req: Request, res: Response, next: NextFunction) {
    next(createError(404));
  }
}

export default NotFoundHandler;
