import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
} from "sequelize-typescript";

export interface DepartmentModel {
  uuid?: string;
  name?: string;
  desc?: string;
}

@Table({
  tableName: "department",
  timestamps: true,
})
export class Department extends Model<DepartmentModel> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  desc!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;
}
