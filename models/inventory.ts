import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
} from "sequelize-typescript";

export interface InventoryModel {
  uuid?: string;
  product_name?: string;
  count?: string;
  unit?: string;
  location?: string;
  orderNumber?: string;
  ship_name?: string;
  status?: string;
}

export class DynamicTableName {
  public static initialize(sequelize: Sequelize, location: string) {
    const tableName = `inventory_${location}`;

    @Table({
      tableName,
    })
    class DynamicInventoryModel extends Inventory {}

    sequelize.addModels([DynamicInventoryModel]);
    return DynamicInventoryModel;
  }
}
@Table
export class Inventory extends Model<InventoryModel> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  product_name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  count!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  location!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  ship_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  status!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;
}
