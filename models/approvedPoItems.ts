import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  BelongsTo,
  ForeignKey,
  Index,
} from "sequelize-typescript";
import { ApprovedPo } from "./approvedPo";

export interface ApprovedPoItemsModel {
  uuid?: string;
  approvedPurchaseOrderId?: string;
  description?: string;
  quantity?: string;
  unit?: string;
  unitPrice?: string;
  amount?: string;
  tax?: string;
  discount?: string;
  shipName?: string;
  status?: string;
  inboundStatus?: string;
}

@Table({
  tableName: "apr_po_items",
  timestamps: true,
})
export class ApprovedPoItems extends Model<ApprovedPoItemsModel> {
  @Index
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  })
  uuid!: number;

  @Index
  @ForeignKey(() => ApprovedPo)
  @Column({ type: DataType.STRING, allowNull: true })
  apr_po_id!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  description!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  quantity!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  amount!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  tax!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  discount!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  ship_name!: string;

  @Column({ type: DataType.STRING, allowNull: true, defaultValue: "Open" })
  status!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @BelongsTo(() => ApprovedPo, "apr_po_id")
  purchaseOrder!: ApprovedPo;
}
