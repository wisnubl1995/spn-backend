import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  BelongsTo,
  ForeignKey,
  Index,
} from "sequelize-typescript";
import { MatrixOfComparison } from "./matrixOfComparison";

export interface MOCItemsModel {
  uuid?: string;
  mocId?: string;
  purchaseRequestNo?: string;
  description?: string;
  unit?: string;
  quantity?: string;
  sapCode?: string;
  shopOneName?: string;
  shopOneUnitPrice?: string;
  shopOneTotalPirce?: string;
  shopOneRemarks?: string;
  shopTwoName?: string;
  shopTwoUnitPrice?: string;
  shopTwoTotalPrice?: string;
  shopTwoRemarks?: string;
  shopThreeName?: string;
  shopThreeUnitPrice?: string;
  shopThreeTotalPrice?: string;
  shopThreeRemarks?: string;
  shopThreeTax?: string;
  status?: string;
  l1Status?: string;
  l2Status?: string;
  l3Status?: string;
  approvedL2By?: string;
  approvedL3By?: string;
}

@Table({
  tableName: "moc_item",
  timestamps: true,
})
export class mocItems extends Model<MOCItemsModel> {
  @Index
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  })
  uuid!: string;

  @Index
  @ForeignKey(() => MatrixOfComparison)
  @Column({ type: DataType.STRING, allowNull: true })
  moc_id!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: true })
  purchase_request_no!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  description!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  quantity!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  sap_code!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_unit_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_total_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_remarks!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_tax!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_unit_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_total_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_remarks!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_tax!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_unit_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_total_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_remarks!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_tax!: string;

  @Column({ type: DataType.STRING, allowNull: true, defaultValue: "Open" })
  status!: string;

  @Column({ type: DataType.BOOLEAN, allowNull: true, defaultValue: false })
  l1_status!: boolean;

  @Column({ type: DataType.BOOLEAN, allowNull: true, defaultValue: false })
  l2_status!: boolean;

  @Column({ type: DataType.BOOLEAN, allowNull: true, defaultValue: false })
  l3_status!: boolean;

  @Column({ type: DataType.STRING, allowNull: true })
  approved_l2_by!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  approved_l3_by!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;
}
