import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  HasMany,
  Index,
} from "sequelize-typescript";
import { PurchaseItem } from "./purchaseItems";

export interface PurchaseRequestModel {
  uuid?: string;
  shipName?: string;
  department?: string;
  category?: string;
  purchaseRequestNo?: string;
  purchaseRequestId?: string;
  requestedBy?: string;
  location?: string;
  status?: string;
  createdBy?: string;
}

@Table({
  tableName: "purchase_request",
  timestamps: true,
})
export class PurchaseRequest extends Model<PurchaseRequestModel> {
  @Index
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  ship_name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  department!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  category!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: false, unique: true })
  purchase_request_no!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  requested_by!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  location!: string;

  @Column({ type: DataType.STRING, allowNull: false, defaultValue: "Open" })
  status!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  created_by!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @HasMany(() => PurchaseItem, "purchase_request_id")
  purchaseItems!: PurchaseItem[];
}
