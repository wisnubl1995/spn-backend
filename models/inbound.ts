import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
} from "sequelize-typescript";

export interface InboundModel {
  uuid?: string;
  product_name?: string;
  count?: string;
  location?: string;
  orderNumber?: string;
  ship_name?: string;
  unit?: string;
  status?: string;
}

export class DynamicTableName {
  public static initialize(sequelize: Sequelize, location: string) {
    const tableName = `inbound_${location}`;

    @Table({
      tableName,
    })
    class DynamicInboundModel extends Inbound {}

    sequelize.addModels([DynamicInboundModel]);
    return DynamicInboundModel;
  }
}
@Table
export class Inbound extends Model<InboundModel> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  product_name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  count!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  location!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  ship_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  order_number!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  status!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;
}
