import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  Index,
  HasMany,
} from "sequelize-typescript";
import { OutboundItems } from "./outboundItems";

export interface OutboundModel {
  uuid?: string;
  noPr?: string;
  shipName?: string;
  deliverTo?: string;
  via?: string;
  noResi?: string;
  location?: string;
  status?: string;
}

export class DynamicTableName {
  public static initialize(sequelize: Sequelize, location: string) {
    const tableName = `outbound_${location}`;

    @Table({
      tableName,
    })
    class DynamicInboundModel extends Outbound {}

    sequelize.addModels([DynamicInboundModel]);
    return DynamicInboundModel;
  }
}
@Table
export class Outbound extends Model<OutboundModel> {
  @Index
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  no_pr!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  ship_name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  deliver_to!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  via!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  no_resi!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  location!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  status!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @HasMany(() => OutboundItems, "outbound_id")
  outboundItems!: OutboundItems[];
}
