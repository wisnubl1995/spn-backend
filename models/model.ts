export type Constructor<T> = new (...args: any[]) => { [K in keyof T]: any };

// Define a generic mapping function

export function mapToInterface<T>(
  data: any[],
  interfaceConstructor: Constructor<T>
): T[] {
  // Get the property names of the interface
  const properties = Object.keys(new interfaceConstructor()) as (keyof T)[];

  return data.map((item: any) => {
    const mappedItem: Partial<T> = {};
    for (const key of properties) {
      if (item.hasOwnProperty(key)) {
        mappedItem[key] = item[key];
      }
    }
    return mappedItem as T;
  });
}
