import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  BelongsTo,
  ForeignKey,
  Index,
} from "sequelize-typescript";

import { Outbound } from "./outbound";

export interface OutboundItemsModel {
  uuid?: string;
  productName?: string;
  unit?: string;
  count?: string;
  additionalInfo?: string;
  status?: string;
}

@Table({
  tableName: "outbound_items",
  timestamps: true,
})
export class OutboundItems extends Model<OutboundItemsModel> {
  @Index
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  })
  uuid!: number;

  @Index
  @ForeignKey(() => Outbound)
  @Column({ type: DataType.STRING, allowNull: true })
  outbound_id!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  product_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  count!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  additional_info!: string;

  @Column({ type: DataType.STRING, allowNull: true, defaultValue: "Open" })
  status!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @BelongsTo(() => Outbound, "outbound_id")
  outbound!: Outbound;
}
