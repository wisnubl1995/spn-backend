import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  BelongsTo,
  ForeignKey,
  Index,
} from "sequelize-typescript";
import { PurchaseOrder } from "./purchaseOrder";

export interface PurchaseOrderItemModel {
  uuid?: string;
  purchaseOrderId?: string;
  description?: string;
  quantity?: string;
  unit?: string;
  unitPrice?: string;
  discount?: string;
  amount?: string;
  tax?: string;
  shipName?: string;
  mocId?: string;
  status?: string;
  shopOneName?: string;
  shopOneUnitPrice?: string;
  shopOneTotalPirce?: string;
  shopOneRemarks?: string;
  shopOneTax?: string;
  shopTwoName?: string;
  shopTwoUnitPrice?: string;
  shopTwoTotalPrice?: string;
  shopTwoRemarks?: string;
  shopTwoTax?: string;
  shopThreeName?: string;
  shopThreeUnitPrice?: string;
  shopThreeTotalPrice?: string;
  shopThreeRemarks?: string;
  shopThreeTax?: string;
  selectedShop?: string;
}

@Table({
  tableName: "po_items",
  timestamps: true,
})
export class PurchaseOrderItem extends Model<PurchaseOrderItemModel> {
  @Index
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  })
  uuid!: number;

  @Index
  @ForeignKey(() => PurchaseOrder)
  @Column({ type: DataType.STRING, allowNull: true })
  po_id!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  description!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  quantity!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  discount!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  amount!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  tax!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  additional_info!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  image!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_unit_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_total_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_remarks!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_one_tax!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_unit_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_total_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_remarks!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_two_tax!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_unit_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_total_price!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_remarks!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  shop_three_tax!: string;

  @Column({ type: DataType.STRING, allowNull: true, defaultValue: "Open" })
  status!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  selected_shop!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @BelongsTo(() => PurchaseOrder, "po_id")
  purchaseOrder!: PurchaseOrder;
}
