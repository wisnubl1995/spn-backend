import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  HasMany,
  Index,
} from "sequelize-typescript";
import { ApprovedPoItems } from "./approvedPoItems";

export interface ApprovedPoModel {
  uuid?: string;
  billingTo?: string;
  quotationNumber?: string;
  department?: string;
  project?: string;
  orderNumber?: string;
  orderDate?: string;
  status?: string;
  purchaseRequestNo?: string;
  poNo?: string;
  shipName?: string;
  createdBy?: string;
  location?: string;
}

@Table({
  tableName: "approved_po",
  timestamps: true,
})
export class ApprovedPo extends Model<ApprovedPoModel> {
  @Index
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  billing_to!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  quotation_number!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  department!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  project!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: true })
  order_number!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  order_date!: string;

  @Column({ type: DataType.STRING, allowNull: true, defaultValue: "Open" })
  status!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: true })
  purchase_request_no!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: true })
  po_no!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  ship_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  created_by!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  location!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @HasMany(() => ApprovedPoItems, "apr_po_id")
  approvedPoItems!: ApprovedPoItems[];
}
