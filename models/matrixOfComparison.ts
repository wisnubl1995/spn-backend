import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  HasMany,
  Index,
} from "sequelize-typescript";
import { mocItems } from "./mocItems";

export interface MatrixOfComparisonModel {
  uuid?: string;
  shipName?: string;
  department?: string;
  category?: string;
  purchaseRequestNo?: string;
  location?: string;
  urgency?: string;
  status?: string;
}

@Table({
  tableName: "matrix_of_comparison",
  timestamps: true,
})
export class MatrixOfComparison extends Model<MatrixOfComparisonModel> {
  @Index
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  ship_name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  department!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  category!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: false })
  purchase_request_no!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  urgency!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  location!: string;

  @Column({ type: DataType.STRING, allowNull: false, defaultValue: "Open" })
  status!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @HasMany(() => mocItems, "moc_id")
  mocItems!: mocItems[];
}
