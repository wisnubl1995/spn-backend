import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  BelongsTo,
  ForeignKey,
  Index,
} from "sequelize-typescript";
import { PurchaseRequest } from "./purchaseRequest";

export interface PurchaseItemModel {
  uuid?: string;
  purchaseRequestId?: string;
  description?: string;
  unit?: string;
  quantityAsked?: string;
  quantityLeft?: string;
  quantityApproved?: string;
  sapCode?: string;
  additionalInfo?: string;
  image?: string;
  status?: string;
  isApproved?: boolean;
  quantityApprovedBy?: string;
  approvedBy?: string;
}

@Table({
  tableName: "purchase_item",
  timestamps: true,
})
export class PurchaseItem extends Model<PurchaseItemModel> {
  @Index
  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  })
  uuid!: string;

  @Index
  @ForeignKey(() => PurchaseRequest)
  @Column({ type: DataType.STRING, allowNull: true })
  purchase_request_id!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  unit!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  quantity_asked!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  quantity_left!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  quantity_approved!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  sap_code!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  description!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  additional_info!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  image!: string;

  @Column({ type: DataType.STRING, allowNull: true, defaultValue: "Open" })
  status!: string;

  @Column({ type: DataType.BOOLEAN, allowNull: true, defaultValue: false })
  is_approved!: boolean;

  @Column({ type: DataType.STRING, allowNull: true })
  quantity_approved_by!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  approved_by!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @BelongsTo(() => PurchaseRequest, "purchase_request_id")
  purchaseRequest!: PurchaseRequest;
}
