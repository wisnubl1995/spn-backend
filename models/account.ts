import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  BelongsTo,
  HasMany,
  ForeignKey,
} from "sequelize-typescript";
import { Role } from "./role";
import { Module } from "./module";

export interface AccountModel {
  username?: string;
  password?: string;
  role_id?: string;
  name?: string;
  uuid?: string;
  status?: number;
  idEmployee?: string;
  division?: string;
  last_login?: string;
  ltree?: string;
  location?: number;
}

@Table({
  tableName: "account",
  timestamps: true,
})
export class Account extends Model<AccountModel> {
  @Column({ type: DataType.STRING, allowNull: false })
  username!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  password!: string;

  @ForeignKey(() => Role)
  @Column({ type: DataType.STRING, allowNull: false })
  role_id!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  id_employee!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  division!: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.INTEGER, allowNull: false, defaultValue: 0 })
  status!: number;

  @Column({ type: DataType.STRING, allowNull: false })
  location!: string;

  @Column({ type: DataType.STRING })
  last_login!: string;

  @Column({ type: DataType.STRING })
  ltree!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @BelongsTo(() => Role, "role_id")
  role!: Role;

  @HasMany(() => Module, "account_id")
  modules!: Module[];
}
