import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  BelongsTo,
  ForeignKey,
} from "sequelize-typescript";
import { Account } from "./account";

export interface ModuleModel {
  uuid?: string;
  moduleName?: string;
  moduleRouter?: string;
  accountId?: string;
}

@Table({
  tableName: "module",
  timestamps: true,
})
export class Module extends Model<ModuleModel> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  module_name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  module_router!: string;

  @ForeignKey(() => Account)
  @Column({ type: DataType.STRING, allowNull: false })
  account_id!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @BelongsTo(() => Account, "account_id")
  account!: Account;
}
