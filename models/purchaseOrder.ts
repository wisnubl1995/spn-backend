import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
  Sequelize,
  BelongsTo,
  ForeignKey,
  HasMany,
  Index,
} from "sequelize-typescript";
import { PurchaseOrderItem } from "./poItems";

export interface PurchaseOrderModel {
  uuid?: string;
  billingTo?: string;
  quotationNumber?: string;
  department?: string;
  project?: string;
  orderNumber?: string;
  orderDate?: string;
  status?: string;
  purchaseRequestNo?: string;
  purchaseOrderNo?: string;
  shipName?: string;
  location?: string;
  createdBy?: string;
}

@Table({
  tableName: "purchase_order",
  timestamps: true,
})
export class PurchaseOrder extends Model<PurchaseOrderModel> {
  @Index
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  billing_to!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  quotation_number!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  department!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  project!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  location!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: true })
  order_number!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  order_date!: string;

  @Column({ type: DataType.STRING, allowNull: true, defaultValue: "Open" })
  status!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: true })
  purchase_request_no!: string;

  @Index
  @Column({ type: DataType.STRING, allowNull: true })
  purchase_order_no!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  ship_name!: string;

  @Column({ type: DataType.STRING, allowNull: true })
  created_by!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;

  @HasMany(() => PurchaseOrderItem, "po_id")
  purchaseOrderItem!: PurchaseOrderItem[];
}
