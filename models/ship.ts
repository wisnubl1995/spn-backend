import {
  Table,
  Column,
  Model,
  DataType,
  CreatedAt,
  UpdatedAt,
} from "sequelize-typescript";

export interface ShipModel {
  uuid?: string;
  name?: string;
  ship_code?: string;
  desc?: string;
  image?: string;
  location?: string;
}

@Table({
  tableName: "ship",
  timestamps: true,
})
export class Ship extends Model<ShipModel> {
  @Column({
    type: DataType.STRING,
    allowNull: false,
    autoIncrement: false,
    primaryKey: true,
  })
  uuid!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  name!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  ship_code!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  desc!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  image!: string;

  @Column({ type: DataType.STRING, allowNull: false })
  location!: string;

  @CreatedAt
  @Column({ type: DataType.DATE, allowNull: false })
  created_at!: string;

  @UpdatedAt
  @Column({ type: DataType.DATE, allowNull: true })
  updated_at!: string;
}
