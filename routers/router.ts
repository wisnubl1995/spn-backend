import express, { Router } from "express";
import { AccountRouter } from "./accountRouter";
import { RoleRouter } from "./roleRouter";
import { ModuleRouter } from "./moduleRouter";
import { InboundRouter } from "./inboundRouter";
import { OutboundRouter } from "./outboundRouter";
import { InventoryRouter } from "./inventoryRouter";
import { UnitRouter } from "./unitRouter";
import { CategoryRouter } from "./categoryRouter";
import { DepartmentRouter } from "./departmentRouter";
import { SapCodeRouter } from "./sapCodeRouter";
import { PurchaseRequestRouter } from "./purchaseRequestRouter";
import { PurchaseItemRouter } from "./purchaseItemRouter";
import { MocItemsRouter } from "./mocItemRouter";
import { MocRouter } from "./mocRouter";
import { PurchaseOrderRouter } from "./purchaseOrderRouter";
import { ApprovedPoRouter } from "./approvedPoRouter";

export class MainRouter {
  private router: Router;

  constructor() {
    this.router = express.Router();
    this.initRoutes();
  }

  private initRoutes() {
    const accountRouter = new AccountRouter();
    const roleRouter = new RoleRouter();
    const moduleRouter = new ModuleRouter();
    const inboundRouter = new InboundRouter();
    const outboundRouter = new OutboundRouter();
    const inventoryRouter = new InventoryRouter();
    const unitRouter = new UnitRouter();
    const categoryRouter = new CategoryRouter();
    const departmentRouter = new DepartmentRouter();
    const sapCodeRouter = new SapCodeRouter();
    const purchaseRequestRouter = new PurchaseRequestRouter();
    const purchaseItemRouter = new PurchaseItemRouter();
    const mocItemsRouter = new MocItemsRouter();
    const mocRouter = new MocRouter();
    const purchaseOrderRouter = new PurchaseOrderRouter();
    const approvedPoRouter = new ApprovedPoRouter();
    this.router.use("/account", accountRouter.getRouter());
    this.router.use("/role", roleRouter.getRouter());
    this.router.use("/module", moduleRouter.getRouter());
    this.router.use("/inbound", inboundRouter.getRouter());
    this.router.use("/outbound", outboundRouter.getRouter());
    this.router.use("/inventory", inventoryRouter.getRouter());
    this.router.use("/unit", unitRouter.getRouter());
    this.router.use("/category", categoryRouter.getRouter());
    this.router.use("/department", departmentRouter.getRouter());
    this.router.use("/sapcode", sapCodeRouter.getRouter());
    this.router.use("/purchaserequest", purchaseRequestRouter.getRouter());
    this.router.use("/purchaseitems", purchaseItemRouter.getRouter());
    this.router.use("/mocitems", mocItemsRouter.getRouter());
    this.router.use("/matrixofcomparison", mocRouter.getRouter());
    this.router.use("/purchaseorder", purchaseOrderRouter.getRouter());
    this.router.use("/approvedpo", approvedPoRouter.getRouter());
  }

  getRouter(): Router {
    return this.router;
  }
}
