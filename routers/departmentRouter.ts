import express, { Router } from "express";
import { DepartmentController } from "../controllers/departmentController";

export class DepartmentRouter {
  private router: Router;
  private controller: DepartmentController;
  constructor() {
    this.router = express.Router();
    this.controller = new DepartmentController();
    this.setupRoutes();
  }

  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/:params", this.controller.search.bind(this.controller));
    this.router.post("/", this.controller.create.bind(this.controller));
    this.router.post("/update", this.controller.update.bind(this.controller));
    this.router.delete("/:id", this.controller.delete.bind(this.controller));
  }

  getRouter() {
    return this.router;
  }
}
