import express, { Router } from "express";
import { InventoryController } from "../controllers/inventoryController";
import { uploadPhoto } from "../middleware/multer";

export class InventoryRouter {
  private router: Router;
  private controller: InventoryController;
  constructor() {
    this.router = express.Router();
    this.controller = new InventoryController();
    this.setupRoutes();
  }

  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/:params", this.controller.search.bind(this.controller));
    this.router.post(
      "/",
      uploadPhoto.array("images", 5),
      this.controller.create.bind(this.controller)
    );
    this.router.post("/update", this.controller.update.bind(this.controller));
    this.router.delete("/:id", this.controller.delete.bind(this.controller));
  }

  getRouter() {
    return this.router;
  }
}
