import express, { Router } from "express";
import { RoleController } from "../controllers/roleController";

export class RoleRouter {
  private router: Router;
  private controller: RoleController;
  constructor() {
    this.router = express.Router();
    this.controller = new RoleController();
    this.setupRoutes();
  }

  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/:id", this.controller.searchId.bind(this.controller));
    this.router.get(
      "/search/:params",
      this.controller.searchName.bind(this.controller)
    );
    this.router.post("/", this.controller.create.bind(this.controller));
    this.router.patch("/update", this.controller.update.bind(this.controller));
    this.router.delete("/:id", this.controller.delete.bind(this.controller));
  }

  getRouter() {
    return this.router;
  }
}
