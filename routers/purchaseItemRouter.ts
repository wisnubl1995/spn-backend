import express, { Router } from "express";
import { uploadPhoto } from "../middleware/multer";
import { PurchaseItemController } from "../controllers/purchaseItemController";

export class PurchaseItemRouter {
  private router: Router;
  private controller: PurchaseItemController;
  constructor() {
    this.router = express.Router();
    this.controller = new PurchaseItemController();
    this.setupRoutes();
  }
  private setupRoutes() {
    this.router.get("/:id", this.controller.getById.bind(this.controller));
    this.router.patch("/update", this.controller.update.bind(this.controller));
    this.router.delete("/:id", this.controller.delete.bind(this.controller));
    this.router.post(
      "/movetomoc",
      this.controller.moveItemToMOC.bind(this.controller)
    );
  }

  getRouter() {
    return this.router;
  }
}
