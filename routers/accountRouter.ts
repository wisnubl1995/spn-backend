import express, { Router } from "express";
import { AccountController } from "../controllers/accountController";

export class AccountRouter {
  private router: Router;
  private controller: AccountController;
  constructor() {
    this.router = express.Router();
    this.controller = new AccountController();
    this.setupRoutes();
  }

  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/:params", this.controller.search.bind(this.controller));
    this.router.post("/", this.controller.create.bind(this.controller));
    this.router.post("/login", this.controller.login.bind(this.controller));
    this.router.post("/update", this.controller.update.bind(this.controller));
  }

  getRouter() {
    return this.router;
  }
}
