import express, { Router } from "express";
import { PurchaseOrderController } from "../controllers/purchaseOrderController";

export class PurchaseOrderRouter {
  private router: Router;
  private controller: PurchaseOrderController;
  constructor() {
    this.router = express.Router();
    this.controller = new PurchaseOrderController();
    this.setupRoutes();
  }
  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/:id", this.controller.getById.bind(this.controller));
    this.router.post(
      "/movepotoaprpo",
      this.controller.movePoToAprPo.bind(this.controller)
    );
  }

  getRouter() {
    return this.router;
  }
}
