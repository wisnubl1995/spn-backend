import express, { Router } from "express";
import { InboundController } from "../controllers/inboundController";

export class InboundRouter {
  private router: Router;
  private controller: InboundController;
  constructor() {
    this.router = express.Router();
    this.controller = new InboundController();
    this.setupRoutes();
  }

  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/:params", this.controller.search.bind(this.controller));
    this.router.post("/", this.controller.create.bind(this.controller));
    this.router.post(
      "/movetoinventory",
      this.controller.moveToInventory.bind(this.controller)
    );
    this.router.patch("/update", this.controller.update.bind(this.controller));
    this.router.delete("/:id", this.controller.delete.bind(this.controller));
  }

  getRouter() {
    return this.router;
  }
}
