import express, { Router } from "express";
import { uploadPhoto } from "../middleware/multer";
import { MocController } from "../controllers/mocController";

export class MocRouter {
  private router: Router;
  private controller: MocController;
  constructor() {
    this.router = express.Router();
    this.controller = new MocController();
    this.setupRoutes();
  }
  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/:id", this.controller.getById.bind(this.controller));
  }

  getRouter() {
    return this.router;
  }
}
