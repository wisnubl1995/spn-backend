import express, { Router } from "express";
import { ApprovedPoController } from "../controllers/approvedPoController";

export class ApprovedPoRouter {
  private router: Router;
  private controller: ApprovedPoController;
  constructor() {
    this.router = express.Router();
    this.controller = new ApprovedPoController();
    this.setupRoutes();
  }
  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/:id", this.controller.getById.bind(this.controller));
    this.router.post(
      "/",
      this.controller.moveAprPoToInbound.bind(this.controller)
    );
  }

  getRouter() {
    return this.router;
  }
}
