import express, { Router } from "express";
import { PurchaseRequestController } from "../controllers/purchaseRequestController";
import { uploadPhoto } from "../middleware/multer";

export class PurchaseRequestRouter {
  private router: Router;
  private controller: PurchaseRequestController;
  constructor() {
    this.router = express.Router();
    this.controller = new PurchaseRequestController();
    this.setupRoutes();
  }

  private setupRoutes() {
    this.router.get("/", this.controller.getAll.bind(this.controller));
    this.router.get("/search", this.controller.search.bind(this.controller));
    this.router.get("/:id", this.controller.getById.bind(this.controller));
    this.router.post(
      "/",
      uploadPhoto.array("images", 20),
      this.controller.create.bind(this.controller)
    );
    this.router.post("/update", this.controller.update.bind(this.controller));
    this.router.delete("/:id", this.controller.delete.bind(this.controller));
  }

  getRouter() {
    return this.router;
  }
}
