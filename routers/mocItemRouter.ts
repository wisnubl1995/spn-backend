import express, { Router } from "express";
import { uploadPhoto } from "../middleware/multer";
import { MocItemsController } from "../controllers/mocItemsController";

export class MocItemsRouter {
  private router: Router;
  private controller: MocItemsController;
  constructor() {
    this.router = express.Router();
    this.controller = new MocItemsController();
    this.setupRoutes();
  }
  private setupRoutes() {
    this.router.post(
      "/movemoctopo",
      this.controller.moveItemToPO.bind(this.controller)
    );
    this.router.patch("/update", this.controller.update.bind(this.controller));
  }

  getRouter() {
    return this.router;
  }
}
