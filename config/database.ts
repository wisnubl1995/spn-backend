import { Sequelize } from "sequelize-typescript";
import { Account } from "../models/account";
import { Role } from "../models/role";
import { Module } from "../models/module";
import { Inventory } from "../models/inventory";
import { Ship } from "../models/ship";
import { Category } from "../models/category";
import { Department } from "../models/department";
import { Unit } from "../models/unit";
import { SapCode } from "../models/sapCode";
import { PurchaseRequest } from "../models/purchaseRequest";
import { PurchaseItem } from "../models/purchaseItems";
import { Inbound } from "../models/inbound";
import { Outbound } from "../models/outbound";
import dotenv from "dotenv";
import { MatrixOfComparison } from "../models/matrixOfComparison";
import { mocItems } from "../models/mocItems";
import { PurchaseOrder } from "../models/purchaseOrder";
import { PurchaseOrderItem } from "../models/poItems";
import { ApprovedPo } from "../models/approvedPo";
import { ApprovedPoItems } from "../models/approvedPoItems";
import { OutboundItems } from "../models/outboundItems";
dotenv.config();
export class DatabaseConnection {
  private static instance: DatabaseConnection;
  private sequelize: Sequelize;

  private constructor() {
    this.sequelize = new Sequelize({
      database: process.env.DB_NAME ? process.env.DB_NAME : "spndb",
      username: process.env.DB_USERNAME ? process.env.DB_USERNAME : "spndb",
      password: process.env.DB_PASS ? process.env.DB_PASS : "User@d3v3l0p3r",
      host: process.env.DB_HOST ? process.env.DB_HOST : "localhost",
      dialect: process.env.DB_DIALECT
        ? (process.env.DB_DIALECT as any)
        : "postgres",
      port: process.env.DB_PORT ? parseInt(process.env.DB_PORT) : 5432,
      schema: process.env.DB_SCHEME ? process.env.DB_SCHEME : "spndb",
      models: [
        Account,
        Role,
        Module,
        Ship,
        Inventory,
        Category,
        Department,
        Unit,
        SapCode,
        PurchaseRequest,
        PurchaseItem,
        Inbound,
        Outbound,
        MatrixOfComparison,
        mocItems,
        PurchaseOrder,
        PurchaseOrderItem,
        ApprovedPo,
        ApprovedPoItems,
        OutboundItems,
      ],
      pool: { min: 0, max: 1000 },
    });
  }

  public static getInstance(): DatabaseConnection {
    if (!this.instance) {
      this.instance = new DatabaseConnection();
    }
    return this.instance;
  }

  public async testConnection(): Promise<void> {
    try {
      await this.sequelize.authenticate();
      await this.sequelize.sync();
      console.log("Connection to database has been established successfully.");
    } catch (error) {
      console.error("Unable to connect to the database:", error);
    }
  }

  public getSequelize(): Sequelize {
    return this.sequelize;
  }
}

const databaseInstance = DatabaseConnection.getInstance();

export default databaseInstance;
