import { QueryInterface, Sequelize } from "sequelize";

export class MySequenceMigration {
  public static async MocSequence(
    queryInterface: QueryInterface,
    Sequelize: Sequelize
  ): Promise<void> {
    try {
      // Create a sequence
      await queryInterface.sequelize.query(`
        CREATE SEQUENCE moc_sequence
        START WITH 1
        INCREMENT BY 1
        NO MINVALUE
        NO MAXVALUE
        CACHE 1;
      `);
    } catch (error) {
      console.error("Error creating sequence:", error);
      throw error;
    }
  }

  public static async DropMocSequence(
    queryInterface: QueryInterface,
    Sequelize: Sequelize
  ): Promise<void> {
    try {
      await queryInterface.sequelize.query(`
        DROP SEQUENCE IF EXISTS moc_sequence;
      `);
    } catch (error) {
      console.error("Error dropping sequence:", error);
      throw error;
    }
  }
}
