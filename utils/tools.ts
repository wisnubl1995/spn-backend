import bcrypt from "bcryptjs";
import { v4 } from "uuid";
import moment, { Moment } from "moment";

export class Tools {
  public hashPassword(password: string) {
    return bcrypt.hash(password, 10);
  }
  public async generateUUID() {
    return v4();
  }
  public comparePassword(hashPassword: string, password: string) {
    return bcrypt.compareSync(password, hashPassword);
  }

  public snakeToCamel(obj: any): any {
    if (typeof obj !== "object" || obj === null) {
      return obj;
    }

    if (Array.isArray(obj)) {
      return obj.map((v) => this.snakeToCamel(v));
    }

    return Object.keys(obj).reduce((acc, key) => {
      let camelKey = key.replace(/(_\w)/g, (m) => m[1].toUpperCase());
      camelKey = camelKey.charAt(0).toLowerCase() + camelKey.slice(1);
      if (obj[key] instanceof Date) {
        acc[camelKey] = obj[key];
      } else {
        acc[camelKey] = this.snakeToCamel(obj[key]);
      }
      return acc;
    }, {} as any);
  }

  public generateUnique() {
    const uuid = v4().substring(0, 4);

    const formatDate = moment().format("DD/MM/YY");

    const randomNumber = Math.floor(Math.random() * 1000000);

    const uniqueString =
      `PO${uuid}-${formatDate}-${randomNumber}`.toUpperCase();

    return uniqueString;
  }
}
