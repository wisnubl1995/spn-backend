/**
 * Convert camelCase to underscore_case
 * @param {string} str - string to be converted
 * @param {boolean} upperCase - whether to convert to upper case (default: true)
 * @return {string} - converted string
 */
function convertCamelCaseToUnderscoreCase(
  str: string,
  upperCase: boolean = true
): string {
  if (upperCase) {
    return str.replace(/([A-Z])/g, "_$1").toUpperCase();
  } else {
    return str.replace(/([A-Z])/g, "_$1").toLowerCase();
  }
}

function stringTemplate(strings: TemplateStringsArray, ...keys: string[]) {
  return function (...values: any[]) {
    const dict: { [key: string]: any } = values[values.length - 1] || {};
    let result = [strings[0]];

    keys.forEach((key, i) => {
      const value = dict[key];
      result.push(value, strings[i + 1]);
    });

    return result.join("");
  };
}

/**
 * Convert phoneNumber with Country Code
 * @param {string} str - string to be converted
 * @return {string} - converted string
 */
function convertPhoneNumberWithCountryCode(str: string): string {
  if (str) {
    str = str.replace(/[^\d+]+/g, "");
    str = str.replace(/^0/, "+62");
    if (str.match(/^62/)) str = "+" + str;
    if (!str.match(/^\+/)) str = "+62" + str;
  }

  return str;
}

function isJSON(str: string): boolean {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export {
  convertCamelCaseToUnderscoreCase,
  convertPhoneNumberWithCountryCode,
  stringTemplate,
  isJSON,
};
