import _ from "lodash";
import { convertCamelCaseToUnderscoreCase } from "./string";

/**
 * Prepare object to be returned as an API response
 * @param {Object} data - object whose attributes will be removed
 * @param  {string[]} attrs - attributes that will be removed
 * @return {Object} - object whose attributes have been removed
 */
function prepareObjectAsAPIResponse(
  data: Record<string, any>,
  ...attrs: string[]
): Record<string, any> {
  // Remove unwanted attributes
  // data = removeObjectAttributes(data, ...attrs);
  // Transform object attribute keys to camelCase
  data = _.transform(data, (res, val, key) => (res[_.camelCase(key)] = val));
  return data;
}

/**
 * Remove specified attributes from an object
 * @param {Object} data - object whose attributes will be removed
 * @param  {string[]} attrs - attributes that will be removed
 * @return {Object} - object whose attributes have been removed
 */
function removeObjectAttributes(
  data: Record<string, any>,
  ...attrs: string[]
): Record<string, any> {
  attrs.forEach((attr) => delete data[attr]);
  return data;
}

/**
 * Transform all attribute keys in an object to pascal case
 * @param {Object} data
 * @param {boolean} upperCase - whether to convert to upper case (default: false)
 * @return {Object}
 */
function transformAttributesToPascalCase(
  data: Record<string, any>,
  upperCase: boolean = false
): Record<string, any> {
  return _.transform(
    data,
    (res, val, key) =>
      (res[convertCamelCaseToUnderscoreCase(key, upperCase)] = val)
  );
}

function clean(obj: any): any {
  return removeEmptyObject(deleteNilValue(obj));
}

function deleteNilValue(obj: any): any {
  _.each(obj, (value, key) => {
    if (_.isObject(value) && !(value instanceof Date)) {
      deleteNilValue(value);
    }
    if (value === null || value === undefined) {
      delete obj[key];
    }
  });
  return obj;
}

function removeEmptyObject(obj: any): any {
  if (_.isArray(obj)) {
    return _(obj)
      .filter(_.isObject)
      .map(removeEmptyObject)
      .reject(_.isEmpty)
      .concat(_.reject(obj, _.isObject))
      .value();
  }
  if (obj instanceof Date) {
    return obj;
  }
  return _(obj)
    .pickBy(_.isObject)
    .mapValues(removeEmptyObject)
    .omitBy(_.isEmpty)
    .assign(_.omitBy(obj, _.isObject))
    .value();
}

export { clean, prepareObjectAsAPIResponse, transformAttributesToPascalCase };
